﻿using System;
using System.Collections.Generic;
using Leafing.Data.Definition;

namespace DbEntryClassLibrary.Chemistry.Models.Models
{
    [DbTable("DEExaminees")]
    public class DeExaminee : DbObjectModel<DeExaminee>
    {
        [Length(100)]
        [AllowNull]
        public string Guid { get; set; }

        public float Score { get; set; }
        [Length(18)]
        public string CardId { get; set; }

        public int Batch { get; set; }

        public int ResitCount { get; set; }

        public float ResitScore { get; set; }
        public bool IsExamed { get; set; }

        public bool IsSync { get; set; }

        public DateTime? ExamDate { get; set; }

        public bool IsLeak { get; set; }

        public bool IsTemperatureAbnormal { get; set; }

        public bool IsPressureAbnormal { get; set; }

        [AllowNull]
        public string Name { get; set; }
       
        [AllowNull]
        public string Company { get; set; }
        [AllowNull]
        public string FlowName { get; set; }
        [AllowNull]
        public string Content { get; set; }


    }
}

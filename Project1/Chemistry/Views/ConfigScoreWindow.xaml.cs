﻿using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chemistry.Views
{
    /// <summary>
    /// ConfigScoreWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ConfigScoreWindow : MahApps.Metro.Controls.MetroWindow
    {
        public ConfigScoreWindow()
        {
            InitializeComponent();
        }

        private void tvTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var item = tvTree.SelectedItem as PropertyNodeItem;
            Messenger.Default.Send<PropertyNodeItem>(item, MessageToken.SelectFlow);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    /// <summary>
    /// 模式
    /// </summary>
    public enum Model
    {
        /// <summary>
        /// 考试模式
        /// </summary>
        Exam,
        /// <summary>
        /// 训练模式
        /// </summary>
        Practice,
        /// <summary>
        /// 标准训练模式
        /// </summary>
        Standerd
    }

    /// <summary>
    /// 考试状态
    /// </summary>
    public enum ExamState
    {
        空闲,
        考试中,
        考试完成,
        未重置,
        未连接,
        断开连接
    }

    public enum RunModel
    {
        练习模式,
        演示模式,
        考试模式,
    }

    /// <summary>
    /// 运算符
    /// </summary>
    public enum Operator
    {
        大于,
        小于,
        区间,
        无
    }

    public enum EventType
    {
        Information,
        Warning,
        Error
    }

}

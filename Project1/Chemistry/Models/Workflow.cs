﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    //[Serializable]
    public class Workflow
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Index { get; set; }

        public string ClassName { get; set; }
    }

    public class Detail : IDataErrorInfo
    {
        public int Id { get; set; }

        public string Step { get; set; }
        public int Score { get; set; }

        public int Index { get; set; }

        public int WorkFlowId { get; set; }

        public Workflow Workflow { get; set; }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get {
                if (columnName == "Score")
                {                    
                    int tempscore=  -1;
                    if (int.TryParse(Score.ToString(), out tempscore))
                    {
                        return null;
                    }
                    else
                    {
                        return "请输入有效数字";
                    }
                }

                if (columnName == "Step")
                {
                    if (string.IsNullOrEmpty(Step))
                        return "请输入步骤";
                    else
                        return null;
                }
                return null;
            }
        }
    }

    public class Abnormal : IDataErrorInfo
    {
        public int Index { get; set; }
        public int Id { get; set; }

        public string Description { get; set; }
        public int Rate { get; set; }

        public int WorkFlowId { get; set; }
        public Workflow WorkFlow { get; set; }

        public Operator Operator { get; set; }
        public double Value { get; set; }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Rate")
                {
                    int tempscore = -1;
                    if (int.TryParse(Rate.ToString(), out tempscore))
                    {
                        return null;
                    }
                    else
                    {
                        return "请输入有效数字";
                    }
                }

                if (columnName == "Description")
                {
                    if (string.IsNullOrEmpty(Description))
                        return "请输入步骤";
                    else
                        return null;
                }
                return null;
            }
        }
    }

    /// <summary>
    /// 考点类    
    /// </summary>
    public class ExaminationPoint
    {
        public int Index { get; set; }

        public int Id { get; set; }
        public string Description { get; set; }
        public Operator Operator { get; set; }
        public int WorkFlowId { get; set; }
        public Workflow WorkFlow { get; set; }
        public double Value1 { get; set; }

        public double Value2 { get; set; }
        public bool IsChecked { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    public class Signin
    {
        public string accessToken { get; set; }
        public DateTime? date { get; set; }
    }

    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }

        public Signin Signin { get; set; }

        public string LoginErrorText { get; set; }
    }
}

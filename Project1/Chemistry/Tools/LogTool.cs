﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Tools
{
    using log4net.Core;

    public class LogTool
    {
        static ILog log = LogManager.GetLogger("MyLogo4Net");



        public static void WriteErrorLog(string message)
        {
            log.Error(message);
        }

        public static void WriteErrorLog(Exception exception)
        {
            log.Error(exception.Message, exception);
        }

        public static void WriteErrorLog(string message, Exception exception)
        {
            log.Error(message,exception);
        }

        public static void WriteDebugLog(object message)
        {
            log.Debug(message);
        }

        public static void WriteDebugLog(object message, Exception exception)
        {
            log.Debug(message, exception);
        }

        public static void WriteInfoLog(object message)
        {
            log.Info(message);
        }

        public static void WriteInfoLog(object message, Exception exception)
        {
            log.Info(message, exception);
        }

        public static void WriteWarnLog(object message)
        {
            log.Warn(message);
        }

        public static void WriteWarnLog(object message, Exception exception)
        {
            log.Warn(message, exception);
        }

    }
}

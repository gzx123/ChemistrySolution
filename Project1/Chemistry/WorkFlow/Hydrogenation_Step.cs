﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Chemistry.Models;
using OPCAutomation;

namespace Chemistry.WorkFlow
{
    /// <summary>
    /// 加氢工艺流程-分步骤考核
    /// </summary>
    public class Hydrogenation_Step : FlowBaseStep
    {
         #region 字段
        private int _sleepfast = 500;
        private int _sleepslow = 1000;//500
        private static int _relayCommand = 0;
        static int _levelNitro_value =0;// 320;
        static int _levelMethanol_value = 0;// 220;
        static double _pressure_value = 0;//压力
        static double _temperature_value = 22;//温度
        static double _flow_value = 0;//流量

        //增加量
        static bool _isgetoverstate1 = false;
        static bool _isgetoverstate2 = false;
        static bool _isgetoverstate3 = false;
        static double _pressure_decrement;
        static double _pressure_increment ;
        static int _levelNitro_crement ;
        static int _levelMethanol_crement ;
        static double _temperature_creament ;
        static double _temperature_decreament;//1
        static double _flow_crement ;

        private Tools.Tool.FlowCommand _flowcommand;
        string _switch1 = string.Empty;
        string _switch2 = string.Empty;
        string _prefix1 = string.Empty;
        string _prefix2 = string.Empty;
        string _llcmd = string.Empty;
        string _ywcmd = string.Empty;
        string _ylcmd = string.Empty;
        string _wdcmd = string.Empty;
        string _aicmd = string.Empty;
        string _relaycmd = string.Empty;
        int _warning1number ;
        int _warning2number ;
        int _fognumber ;
        int _stirnumber;

        static List<Step> _steps = new List<Step>();

        //流程标识
        static bool _isreplaceN2_over = false;

        int open = 48;//1
        int close = 49;//0

        #endregion

        #region 与组态对应阀门状态
        private static bool ValveInNitro_State = false;
        private static bool ValveInMethanol_State = false;
        private static bool ValveOutNitro_State = false;
        private static bool ValveOutMethanol_State = false;
        private static bool ValveInN2_State = false;
        private static bool ValveEmpty_State = false;
        private static bool ValveCold_State = false;
        private static bool ValveInHot_State = false;
        private static bool ValveInH2_State = false;
        private static bool ValveDischarge_State = false;
        private static bool ValveAutoEmpty_State = false;
        private static bool ValveICutoff_State = false;

        private static bool ValveOverNitro_State = false;
        private static bool ValveOverMethanol_State = false;
        private static bool ValveStir_State = false;//搅拌釜状态
        private static bool ValveStop_State = false;//急停按钮状态
        #endregion

        #region OPCItem
        /// <summary>
        /// 硝基物进料阀
        /// </summary>
        OPCItem ValveInNitro_item;
        /// <summary>
        /// 甲醇进料阀
        /// </summary>
        OPCItem ValveInMethanol_item;
        /// <summary>
        /// 硝基物出料阀
        /// </summary>
        OPCItem ValveOutNitro_item;
        /// <summary>
        /// 甲醇出料阀
        /// </summary>
        OPCItem ValveOutMethanol_item;
        /// <summary>
        /// 氮气阀
        /// </summary>
        OPCItem ValveInN2_item;
        /// <summary>
        /// 放空阀
        /// </summary>
        OPCItem ValveEmpty_item;
        /// <summary>
        /// 冷水阀
        /// </summary>
        OPCItem ValveCold_item;
        /// <summary>
        /// 蒸汽阀
        /// </summary>
        OPCItem ValveInHot_item;
        /// <summary>
        /// 氢气进料阀
        /// </summary>
        OPCItem ValveInH2_item;
        /// <summary>
        /// 放料阀
        /// </summary>
        OPCItem ValveDischarge_item;
        /// <summary>
        /// 自动放空阀
        /// </summary>
        OPCItem ValveAutoEmpty_item;
        /// <summary>
        /// 氢气紧急切断阀
        /// </summary>
        OPCItem ValveICutoff_item;
        /// <summary>
        /// 硝基物溢出阀
        /// </summary>
        OPCItem ValveNitro_item;
        /// <summary>
        /// 甲醇溢出阀
        /// </summary>
        OPCItem ValveMethanol_item;

        private OPCItem ValveStir_item;
        private OPCItem ValveStop_item;

        //label
        OPCItem LabelNitro_item;
        OPCItem LabelMethanol_item;
        OPCItem LabelPressure_item;
        OPCItem LabelTemperature_item;
        OPCItem LabelFlow_item;
        OPCItem LabelOxygen_item;

        //闪烁
        OPCItem FlashCatalyst_item;
        OPCItem FlashStir_item;
        OPCItem FlashPressure_item;
        OPCItem FlashTemperature_item;
        OPCItem FlashWarning_item;

        //管道流动item
        OPCItem FlowInNitro_item;
        OPCItem FlowInMethanol_item;
        OPCItem FlowOutMethanol_item;
        OPCItem FlowOutNitro_item;
        OPCItem FlowOutEmpty_item;
        OPCItem FlowInCold_item;
        OPCItem FlowInHot_item;
        OPCItem FlowInH2_item;
        OPCItem FlowDischarge_item;
        OPCItem FlowInN2_item;

        #endregion

        #region 初始化
         public Hydrogenation_Step(OPCServer opcserver, SerialPort port, bool isinner,List<ExaminationPoint> points)
        {
            _opcServer = opcserver;
            _serialPort = port;
            Isinner = isinner;
            base._points = points;
            
            InitFiled();
            InitOPC();
            InitOpcItem();
            InitSignal();
        }


        private void InitFlowCommandData()
        {
            string classname = this.GetType().Name;
            classname = classname.Substring(0, classname.Length - 5);
            _flowcommand = Tools.Tool.GetFlowCommand(classname);
            _switch1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch1").Value;
            _switch2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch2").Value;
            _prefix1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix1").Value;
            _prefix2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix2").Value;
            _llcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "llcmd").Value;
            _ywcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ywcmd").Value;
            _ylcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ylcmd").Value;
            _wdcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "wdcmd").Value;
            _aicmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "aicmd").Value;
            _relaycmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "relaycmd").Value;
            _warning1number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning1").Value);
            _warning2number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning2").Value);
            _fognumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "fog").Value);
            _stirnumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "stir").Value);
        }

        private static void InitFiled()
        {
            _isgetoverstate1 = false;
            _isgetoverstate2 = false;
            _isgetoverstate3 = false;
            _relayCommand = 0;
            _levelNitro_value = 340;//340;
            _levelMethanol_value = 240;// 240;
            _pressure_value = 0;//压力
            _temperature_value = 22;//温度
            _flow_value = 0;//流量

            _pressure_decrement = 0.02;//0.02
            _pressure_increment = 0.02;//0.01
            _levelNitro_crement = 10;
            _levelMethanol_crement = 10;
            _temperature_creament = 1;//2
            _temperature_decreament = 1;//1
            _flow_crement = 1;
        }

        private void InitOpcItem()
        {
            ValveOutNitro_item = GetItem(FiledName.ValveOutNitro); ValveOutNitro_item.Close();
            ValveOutMethanol_item = GetItem(FiledName.ValveOutMethanol); ValveOutMethanol_item.Close();
            ValveInN2_item = GetItem(FiledName.ValveInN2); ValveInN2_item.Close();
            ValveEmpty_item = GetItem(FiledName.ValveEmpty); ValveEmpty_item.Open();
            ValveCold_item = GetItem(FiledName.ValveCold); ValveCold_item.Close();
            ValveInHot_item = GetItem(FiledName.ValveInHot); ValveInHot_item.Close();
            ValveInH2_item = GetItem(FiledName.ValveInH2); ValveInH2_item.Close();
            ValveDischarge_item = GetItem(FiledName.ValveDischarge); ValveDischarge_item.Close();
            ValveAutoEmpty_item = GetItem(FiledName.ValveAutoEmpty); ValveAutoEmpty_item.Close();
            ValveICutoff_item = GetItem(FiledName.ValveICutoff); ValveICutoff_item.Open();
            ValveInNitro_item = GetItem(FiledName.ValveInNitro); ValveInNitro_item.Close();
            ValveInMethanol_item = GetItem(FiledName.ValveInMethanol); ValveInMethanol_item.Close();
            ValveNitro_item = GetItem(FiledName.ValveOverNitro); ValveNitro_item.Close();
            ValveMethanol_item = GetItem(FiledName.ValveOverMethanol); ValveMethanol_item.Close();
            ValveStir_item = GetItem(FiledName.ValveStir);
            ValveStir_item.Close();
            ValveStop_item = GetItem(FiledName.ValveStop);
            ValveStop_item.Close();

            //管道item
            FlowInNitro_item = GetItem(FiledName.FlowInNitro); FlowInNitro_item.Close();
            FlowInMethanol_item = GetItem(FiledName.FlowInMethanol); FlowInMethanol_item.Close();
            FlowOutMethanol_item = GetItem(FiledName.FlowOutMethanol); FlowOutMethanol_item.Close();
            FlowOutNitro_item = GetItem(FiledName.FlowOutNitro); FlowOutNitro_item.Close();
            FlowOutEmpty_item = GetItem(FiledName.FlowOutEmpty); FlowOutEmpty_item.Close();
            FlowInCold_item = GetItem(FiledName.FlowInCold); FlowInCold_item.Close();
            FlowInHot_item = GetItem(FiledName.FlowInHot); FlowInHot_item.Close();
            FlowInH2_item = GetItem(FiledName.FlowInH2); FlowInH2_item.Close();
            FlowDischarge_item = GetItem(FiledName.FlowDischarge); FlowDischarge_item.Close();
            FlowInN2_item = GetItem(FiledName.FlowInN2); FlowInN2_item.Close();

            //Label
            LabelNitro_item = GetItem(FiledName.LabelNitro); LabelNitro_item.Write(_levelNitro_value);
            LabelMethanol_item = GetItem(FiledName.LabelMethanol); LabelMethanol_item.Write(_levelMethanol_value);
            LabelPressure_item = GetItem(FiledName.LabelPresssure); LabelPressure_item.Write(_pressure_value);
            LabelTemperature_item = GetItem(FiledName.LabelTemperature); LabelTemperature_item.Write(_temperature_value);
            LabelFlow_item = GetItem(FiledName.LabelFlow); LabelFlow_item.Write(0);
            LabelOxygen_item = GetItem(FiledName.LabelOxygen); LabelOxygen_item.Write(21);

            //闪烁item
            FlashCatalyst_item = GetItem(FiledName.FlashCatalyst); FlashCatalyst_item.Close();
            FlashStir_item = GetItem(FiledName.FlashStir); FlashStir_item.Close();
            FlashPressure_item = GetItem(FiledName.FlashPressure);
            FlashPressure_item.Close();
            FlashTemperature_item = GetItem(FiledName.FlashTemperature);
            FlashTemperature_item.Close();
            FlashWarning_item = GetItem(FiledName.FlashWarning);
            FlashWarning_item.Write(0);
        }

        private void InitSignal()
        {
            if (Isinner)
            {
                ListenSignal();
                open = 1;
                close = 0;
            }
            else
            {
                InitFlowCommandData();
                _serialPort.DataReceived += _serialPort_DataReceived;
                InitOpcItemWithSerialPort();
                ListenSerialPort();
                open = 48;
                close = 49;
            }
        }

        private void InitOpcItemWithSerialPort()
        {
            SPYWValue(1, _levelNitro_value);
            SPYWValue(2, _levelMethanol_value);
            SpPressureValue(0.01);
            SpTemperatureValue(_temperature_value);
            Thread.Sleep(200);
            SpFlowValue(0);
            //关闭所有继电器
            _relayCommand = 0;
            SPWriteTool(_relayCommand);
        }

        #endregion

        #region 监听步骤，计分
        private void ListenSerialPort()
        {
            string chkcmd_1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "chkcmd1").Value;  //"#RYW21651&";
            string chkcmd_2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "chkcmd2").Value;//"#RYW21652&";

            Task listSerialPortState = new Task(
                () =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (_serialPort.IsOpen)
                        {
                            lock (this)
                            {
                                Thread.Sleep(150);
                                try
                                {
                                    _serialPort.Write(chkcmd_1);
                                }
                                catch (Exception)
                                { }

                            }
                        }

                        if (_serialPort.IsOpen)
                        {
                            lock (this)
                            {
                                Thread.Sleep(150);
                                try
                                {
                                    _serialPort.Write(chkcmd_2);
                                }
                                catch (Exception)
                                { }

                            }
                        }
                    }
                });

            listSerialPortState.Start();
        }

        int _replace_count1 = 1;
        int _replace_count2 = -1;
        bool _can_open_empty1 = false;
        bool _can_open_empty2 = false;
        bool _isListenSingnalover = false;
        private void ListenSignal()
        {
            Task t = new Task(() =>
            {
                while (!_listenCts.IsCancellationRequested)
                {
                    var valveInNitro = Convert.ToInt32(ReadItem(FiledName.ValveInNitro));
                    var ValveInMethanol = Convert.ToInt32(ReadItem(FiledName.ValveInMethanol));
                    var ValveOutNitro = Convert.ToInt32(ReadItem(FiledName.ValveOutNitro));
                    var ValveOutMethanol = Convert.ToInt32(ReadItem(FiledName.ValveOutMethanol));
                    var ValveInN2 = Convert.ToInt32(ReadItem(FiledName.ValveInN2));
                    var ValveEmpty = Convert.ToInt32(ReadItem(FiledName.ValveEmpty));
                    var ValveCold = Convert.ToInt32(ReadItem(FiledName.ValveCold));
                    var ValveInHot = Convert.ToInt32(ReadItem(FiledName.ValveInHot));
                    var ValveInH2 = Convert.ToInt32(ReadItem(FiledName.ValveInH2));
                    var ValveDischarge = Convert.ToInt32(ReadItem(FiledName.ValveDischarge));
                    var ValveAutoEmpty = Convert.ToInt32(ReadItem(FiledName.ValveAutoEmpty));
                    var ValveICutoff = Convert.ToInt32(ReadItem(FiledName.ValveICutoff));
                    var ValveOverNitro = Convert.ToInt32(ReadItem(FiledName.ValveOverNitro));
                    var ValveOverMethanol = Convert.ToInt32(ReadItem(FiledName.ValveOverMethanol));
                    var ValveStir = Convert.ToInt32(ReadItem(FiledName.ValveStir));
                    var ValveStop = Convert.ToInt32(ReadItem(FiledName.ValveStop));

                    ValveInNitro_State = valveInNitro == open;
                    ValveInMethanol_State = ValveInMethanol == open;
                    ValveOutNitro_State = ValveOutNitro == open;
                    ValveOutMethanol_State = ValveOutMethanol == open;
                    ValveInN2_State = ValveInN2 == open;
                    ValveEmpty_State = ValveEmpty == open;//1
                    ValveCold_State = ValveCold == open;
                    ValveInHot_State = ValveInHot == open;
                    ValveInH2_State = ValveInH2 == open;
                    ValveDischarge_State = ValveDischarge == open;
                    ValveAutoEmpty_State = ValveAutoEmpty == open;
                    ValveICutoff_State = ValveICutoff == open;
                    ValveOverNitro_State = ValveOverNitro == open;
                    ValveOverMethanol_State = ValveOverMethanol == open;
                    ValveStir_State = ValveStir == open;
                    ValveStop_State = ValveStop == open;

                    DateTime dtnow = DateTime.Now;
                    
                    Step step = new Step()
                    {
                        Status =
                    {
                       {StatusName.氮气阀1,-1},
                       {StatusName.氮气阀2,-1},
                       {StatusName.氮气阀3,-1},
                       {StatusName.氮气阀4,-1},

                       {StatusName.放空阀1,-1},
                       {StatusName.放空阀2,-1},
                       {StatusName.放空阀3,-1},
                       {StatusName.放空阀4,-1},
                       
                       {StatusName.硝基物进料阀,valveInNitro},
                       {StatusName.硝基物放料阀,ValveOutNitro},
                       {StatusName.甲醇进料阀,ValveInMethanol},
                       {StatusName.甲醇放料阀,ValveOutMethanol},

                       {StatusName.放料阀,ValveDischarge},
                       {StatusName.甲醇液位, _levelMethanol_value},
                       {StatusName.搅拌阀,ValveStir},
                       {StatusName.急停按钮,ValveStop},
                       {StatusName.紧急切断阀,ValveICutoff},
                       {StatusName.冷水阀,ValveCold},
                       {StatusName.流量,_flow_value},
                       {StatusName.氢气阀,ValveInH2},
                       {StatusName.温度, _temperature_value},
                       {StatusName.硝基物液位,_levelNitro_value},
                       {StatusName.压力,_pressure_value},
                       {StatusName.蒸汽阀, ValveInHot},
                       {StatusName.自动放空阀, ValveAutoEmpty}

                    },
                        Date = dtnow,
                    };

                    FillStep(ValveInN2, ValveEmpty, step);
                    
                    _steps.Add(step);

                    _isListenSingnalover = true;
                    Thread.Sleep(_sleepfast);
                }
            });
            t.Start();

            //ListAbnormal();
        }

        int valveInNitro = -1,
                       valveInMethanol = -1,
                       valveOutNitro = -1,
                       valveOutMethanol = -1,
                       valveStir = -1,
                       valveStop = -1,
                       valveInN2 = -1,
                       valveEmpty = -1,
                       valveCold = -1,
                       valveInHot = -1,
                       valveInH2 = -1,
                       valveDischarge = -1,
                       valveAutoEmpty = -1,
                       valveICutoff = -1;
        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int n = _serialPort.BytesToRead;
            byte[] data = new byte[n];
            _serialPort.Read(data, 0, data.Length);

            string temp = Encoding.Default.GetString(data);
            if (temp.Contains("\n"))
            {
                Match match = Regex.Match(temp, @"\d{5}_\d{3}");
                if (match.Success)
                {
                    Console.WriteLine("串口1收到：\t" + temp);
                    string s = match.Groups[0].Value.Split('_')[1];
                    int v = Convert.ToInt32(s);
                    string r = Convert.ToString(v, 2).PadLeft(8, '0');
                    string first = match.Groups[0].Value.Split('_')[0];

                    if (first == _prefix1)
                    {
                        #region 21651
                        valveOutNitro = Convert.ToInt32(r[0]);
                        valveEmpty = Convert.ToInt32(r[1]);
                        valveStop = Convert.ToInt32(r[2]); //2
                        valveStir = Convert.ToInt32(r[3]); //3
                        valveInMethanol = Convert.ToInt32(r[4]);
                        valveOutMethanol = Convert.ToInt32(r[5]); //5
                        valveInH2 = Convert.ToInt32(r[6]);
                        valveAutoEmpty = Convert.ToInt32(r[7]);

                        ValveOutNitro_State = valveOutNitro == open;
                        ValveEmpty_State = valveEmpty == open;
                        ValveStir_State = valveStir == open;
                        ValveStop_State = valveStop == open;
                        ValveInMethanol_State = valveInMethanol == open;
                        ValveOutMethanol_State = valveOutMethanol == open;
                        ValveAutoEmpty_State = valveAutoEmpty == open;
                        ValveInH2_State = valveInH2 == open;
                        _isgetoverstate1 = true;
                        #endregion
                    }
                    else if (first == _prefix2)
                    {
                        #region 21652
                        valveInHot = Convert.ToInt32(r[1]);
                        valveInNitro = Convert.ToInt32(r[2]);
                        valveDischarge = Convert.ToInt32(r[3]);
                        valveCold = Convert.ToInt32(r[4]);
                        valveICutoff = Convert.ToInt32(r[5]);
                        valveInN2 = Convert.ToInt32(r[6]);

                        ValveInHot_State = valveInHot == open;
                        ValveInNitro_State = valveInNitro == open;
                        ValveDischarge_State = valveDischarge == open;
                        ValveCold_State = valveCold == open;
                        ValveICutoff_State = valveICutoff == open;
                        ValveInN2_State = valveInN2 == open;
                        _isgetoverstate2 = true;
                        #endregion
                    }
                    #region Add Step
                    DateTime dtnow = DateTime.Now;
                    Step step = new Step()
                    {
                        Status = new Dictionary<Enum, double>()
                        {
                            {StatusName.氮气阀1, -1},
                            {StatusName.氮气阀2, -1},
                            {StatusName.氮气阀3, -1},
                            {StatusName.氮气阀4, -1},

                            {StatusName.放空阀1, -1},
                            {StatusName.放空阀2, -1},
                            {StatusName.放空阀3, -1},
                            {StatusName.放空阀4, -1},

                            {StatusName.甲醇液位, _levelMethanol_value},
                            {StatusName.流量, _flow_value},
                            {StatusName.温度, _temperature_value},
                            {StatusName.硝基物液位, _levelNitro_value},
                            {StatusName.压力, _pressure_value},


                            {StatusName.硝基物进料阀, valveInNitro},
                            {StatusName.硝基物放料阀, valveOutNitro},
                            {StatusName.甲醇进料阀, valveInMethanol},
                            {StatusName.甲醇放料阀, valveOutMethanol},
                            {StatusName.搅拌阀, valveStir},
                             {StatusName.急停按钮,valveStop},
                            {StatusName.紧急切断阀, valveICutoff},
                            {StatusName.蒸汽阀, valveInHot},
                            {StatusName.冷水阀, valveCold},
                            {StatusName.氢气阀, valveInH2},
                            {StatusName.自动放空阀, valveAutoEmpty},
                            {StatusName.放料阀, valveDischarge},
                        },
                        Date = dtnow,
                    };
                    FillStep(valveInN2, valveEmpty, step);
                    _steps.Add(step); 
                    #endregion
                }
            }
        }

        private void FillStep(int valveInN2, int valveEmpty, Step step)
        {
            if (_replace_count1 == 1)
            {
                step.Status[StatusName.氮气阀1] = valveInN2;
                if (_can_open_empty1)
                {
                    if (_pressure_value == 0)
                    {
                        step.Status[StatusName.放空阀1] = valveEmpty;
                        LabelOxygen_item.Write(5);
                        _replace_count1 = 2;
                        _can_open_empty1 = false;
                    }
                }
            }
            else if (_replace_count1 == 2)
            {
                step.Status[StatusName.氮气阀2] = valveInN2;
                if (_can_open_empty1)
                {
                    if (_pressure_value == 0 )
                    {
                        step.Status[StatusName.放空阀2] = valveEmpty;
                        LabelOxygen_item.Write(1);
                        _replace_count1 = 3;//之后不再计算
                        _isreplaceN2_over = true;
                    }
                }
            }

            //反应后氮气置换
            if (_replace_count2 == 1)
            {
                step.Status[StatusName.氮气阀3] = valveInN2;
                if (_can_open_empty2)
                {
                    if (_pressure_value == 0)
                    {
                        step.Status[StatusName.放空阀3] = valveEmpty;
                        _replace_count2 = 2;
                        _can_open_empty2 = false;
                    }
                }
            }
            else if (_replace_count2 == 2 )
            {
                step.Status[StatusName.氮气阀4] = valveInN2;
                if (_can_open_empty2)
                {
                    if (_pressure_value == 0)
                    {
                        step.Status[StatusName.放空阀4] = valveEmpty;
                        _replace_count2 = 3;//之后不再计算
                    }
                }
            }
        }

        #endregion

        #region 公共方法

         public override void Begin()
        {
            ListenOpcRelation();
             CheckAbnormalPoint();
        }
        public override async Task<string> CheckDefault()
        {
            StringBuilder builder = new StringBuilder();
              await Task.Run(() =>
              {
                  if (!Isinner)
                  {
                      while (!_isgetoverstate1 || !_isgetoverstate2 )
                      {
                          Thread.Sleep(_sleepslow);
                      }
                  }
                  else
                  {
                      while (!_isListenSingnalover)
                      {
                          Thread.Sleep(_sleepslow);
                      }
                  }
                  
                  if (ValveInNitro_State)
                      builder.AppendLine("硝基物进料阀未关闭");
                  if (ValveInMethanol_State)
                      builder.AppendLine("甲醇进料阀未关闭");
                  if (ValveOutNitro_State)
                      builder.AppendLine("硝基物放料阀未关闭");
                  if (ValveOutMethanol_State)
                      builder.AppendLine("甲醇放料阀未关闭");
                  if (ValveInN2_State)
                      builder.AppendLine("氮气阀未关闭");
                  if (!ValveEmpty_State)
                      builder.AppendLine("手动放空阀未打开");
                  if (ValveCold_State)
                      builder.AppendLine("冷水阀未关闭");
                  if (ValveInHot_State)
                      builder.AppendLine("蒸汽阀未关闭");
                  if (ValveInH2_State)
                      builder.AppendLine("氢气阀未关闭");
                  if (ValveDischarge_State)
                      builder.AppendLine("放料阀未关闭");
                  if (ValveAutoEmpty_State)
                      builder.AppendLine("自动放空阀未关闭");
                  if (!ValveICutoff_State)
                      builder.AppendLine("紧急切断阀未打开");
                  if (ValveStir_State)
                      builder.AppendLine("搅拌按钮未关闭");
                  if (ValveStop_State)
                      builder.AppendLine("急停按钮未关闭");

              });

            return builder.ToString();
        }

        public override void Over()
        {
            Close();
            CalcFlowAndStep();
            Score = CalcScore();
            CloseAllRelay();
            Finish();
        }
        public override void Close()
        {
            this._listenCts.Cancel();
            base.Close();
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= _serialPort_DataReceived;
                _serialPort.Close();
            }
        }
        private void ListenOpcRelation()
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                bool is_reaction_over = false;
                bool can_open_n2 = false;
                #region 搅拌釜状态
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (!ValveStir_State)
                        {
                            StopStir();
                            ValveStir_item.Close();
                        }
                        else
                        {
                            ValveStir_item.Open();
                            BeginStir();

                            FlashStir_item.Open();
                            Thread.Sleep(300);
                            FlashStir_item.Close();
                            Thread.Sleep(300);
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });

                #endregion

                #region 硝基物进料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInNitro_State)
                        {
                            if (!Isinner) ValveInNitro_item.Open();
                            FlowInNitro_item.StartFlow();
                            _levelNitro_value += _levelNitro_crement;
                            if (_levelNitro_value <= 600)
                            {
                                LabelNitro_item.Write(_levelNitro_value);
                                SPYWValue(1, _levelNitro_value);
                            }
                            else if (_levelNitro_value > 650)
                            {
                                BeginWarning(WarningType.硝基物液位过高);
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveInNitro_item.Close();
                            StopWarning(WarningType.硝基物液位过高);
                            FlowInNitro_item.StopFlow();
                            if (_levelNitro_value > 650)
                            {
                                _levelNitro_value = 600;
                                LabelNitro_item.Write(_levelNitro_value);
                                SPYWValue(1, _levelNitro_value);
                            }
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 甲醇进料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInMethanol_State)
                        {
                            if (!Isinner) ValveInMethanol_item.Open();
                            FlowInMethanol_item.StartFlow();
                            _levelMethanol_value += _levelMethanol_crement;

                            if (_levelMethanol_value <= 400)
                            {
                                LabelMethanol_item.Write(_levelMethanol_value);
                                SPYWValue(2, _levelMethanol_value);
                            }
                            else if (_levelMethanol_value > 450)
                            {
                                BeginWarning(WarningType.甲醇液位过高);
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveInMethanol_item.Close();
                            StopWarning(WarningType.甲醇液位过高);
                            FlowInMethanol_item.StopFlow();
                            if (_levelMethanol_value > 450)
                            {
                                _levelMethanol_value = 400;
                                LabelMethanol_item.Write(_levelMethanol_value);
                                SPYWValue(2, _levelMethanol_value);
                            }
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #endregion

                #region 硝基物出料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveOutNitro_State)
                        {
                            if (!Isinner) ValveOutNitro_item.Open();
                            FlowOutNitro_item.StartFlow();
                            _levelNitro_value -= _levelNitro_crement;

                            if (_levelNitro_value < 30)
                            {
                                _levelNitro_crement = 5;
                            }
                            else
                            {
                                _levelNitro_crement = 10;
                            }


                            if (_levelNitro_value >= 0)
                            {
                                LabelNitro_item.Write(_levelNitro_value);
                                SPYWValue(1, _levelNitro_value);
                            }
                            else
                            {
                                _levelNitro_value = 0;
                                FlowOutNitro_item.StopFlow();
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveOutNitro_item.Close();
                            FlowOutNitro_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #endregion

                #region 甲醇出料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveOutMethanol_State)
                        {
                            if (!Isinner) ValveOutMethanol_item.Open();
                            FlowOutMethanol_item.StartFlow();
                            _levelMethanol_value -= _levelMethanol_crement;

                            if (_levelMethanol_value < 30)
                                _levelMethanol_crement = 5;
                            else
                                _levelMethanol_crement = 10;

                            if (_levelMethanol_value >= 0)
                            {
                                SPYWValue(2, _levelMethanol_value);
                                LabelMethanol_item.Write(_levelMethanol_value);
                            }
                            else
                            {
                                _levelMethanol_value = 0;
                                FlowOutMethanol_item.StopFlow();
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveOutMethanol_item.Close();
                            FlowOutMethanol_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #endregion

                #region 氮气阀控制，打开：压力升高
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveInN2_State)
                        {
                            if (!Isinner) ValveInN2_item.Open();
                            FlowInN2_item.StartFlow();

                            _pressure_value += _pressure_increment;
                            _pressure_value = Math.Round(_pressure_value, 2);

                            SpPressureValue(_pressure_value);                            
                            LabelPressure_item.Write(_pressure_value);

                            if (_pressure_value > 0.3)
                            {
                                FlashPressure_item.StartFlow(); //闪烁
                                BeginWarning(WarningType.氮气置换压力过高);
                                _pressure_increment = 0.01;
                            }
                            else
                            {
                                _pressure_increment = 0.02;
                            }

                            if (_pressure_value > 0.2)
                            {
                                _can_open_empty1 = true;
                            }
                            if (can_open_n2 && _pressure_value > 0.2)
                            {
                                _can_open_empty2 = true;
                            }
                        }
                        else
                        {
                           // if (_pressure_value < 0.3)
                            this.StopWarning(WarningType.氮气置换压力过高);
                            if (!Isinner) ValveInN2_item.Close();
                            FlowInN2_item.StopFlow();
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });

                #endregion

                #region 放空阀控制，打开降压力
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveEmpty_State)
                        {
                            if (!Isinner) ValveEmpty_item.Open();
                            FlowOutEmpty_item.StartFlow();

                            _pressure_value = Math.Round(_pressure_value, 2);
                            _pressure_value -= _pressure_decrement;


                            if (_pressure_value < 0.05)
                            {
                                _pressure_decrement = 0.01;
                            }
                            else
                            {
                                _pressure_decrement = 0.04;
                            }

                            if (_pressure_value >= 0.01)
                            {
                                SpPressureValue(_pressure_value);                                
                                LabelPressure_item.Write(_pressure_value);
                            }
                            else
                            {
                                _pressure_value = 0;
                                LabelPressure_item.Write(_pressure_value);
                                FlashPressure_item.StopFlow();

                                //反应完 降到0 才能进行氮气置换
                                can_open_n2 = is_reaction_over;
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveEmpty_item.Close();
                            FlowOutEmpty_item.StopFlow();
                            FlashPressure_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region  加氢，打开：压力升高，流量
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveInH2_State)
                        {
                            if (!Isinner) ValveInH2_item.Open();
                           
                            _flow_value = Math.Round(_flow_value, 2);
                            LabelFlow_item.Write(_flow_value);
                            FlowInH2_item.StartFlow();

                            //控制反应压力 和 流量
                            if (_pressure_value < 1)
                            {
                                _pressure_increment = 0.06;
                                _flow_crement = 0.5;
                            }
                            else if (_pressure_value >= 1 && _pressure_value <= 2)
                            {
                                _pressure_increment = 0.04;
                                _flow_crement = 0.2;
                            }
                            else
                            {
                                _pressure_increment = 0.01;
                                _flow_crement = 0.2;
                                _pressure_decrement = 0.04;
                            }

                            //if (_pressure_value <= 2)
                            {
                                _pressure_value += _pressure_increment;
                                LabelPressure_item.Write(_pressure_value);
                                SpPressureValue(_pressure_value);
                                _flow_value += _flow_crement;                                
                                SpFlowValue(_flow_value);
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveInH2_item.Close();
                            FlowInH2_item.StopFlow();

                            _flow_value = 0;
                            LabelFlow_item.Write(_flow_value);
                            SpFlowValue(_flow_value);
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });
                #endregion

                #region 蒸汽阀控制
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInHot_State)
                        {
                            if (!Isinner) ValveInHot_item.Open();
                            FlowInHot_item.StartFlow();

                            //控制反应温度
                            if (_temperature_value >= 98)
                            {
                                _temperature_creament = 0;
                            }
                            else if (_temperature_value > 85)
                            {
                                _temperature_creament = 0.5;
                            }
                            else if (_temperature_value > 60)
                            {
                                _temperature_creament = 1;//1
                            }
                            else
                            {
                                _temperature_creament = 2;//2
                            }


                            //if (_temperature_value <= 98)
                            {
                                _temperature_value += _temperature_creament;
                                _temperature_value = Math.Round(_temperature_value, 2);

                                SpTemperatureValue(_temperature_value);
                                LabelTemperature_item.Write(_temperature_value);
                            }

                            if (_temperature_value > 110)
                            {
                                BeginWarning(WarningType.温度过高);
                            }
                        }
                        else
                        {
                            StopWarning(WarningType.温度过高);
                            if (!Isinner) ValveInHot_item.Close();
                            FlowInHot_item.StopFlow();
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });
                #endregion

                #region 冷水阀控制
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveCold_State)
                        {
                            if (!Isinner) ValveCold_item.Open();
                            FlowInCold_item.StartFlow();
                            _temperature_value = Math.Round(_temperature_value, 2);
                            _temperature_value -= _temperature_decreament;

                            if (_temperature_value < 22)
                            {
                                _temperature_decreament = 0;
                            }
                            else if (_temperature_value < 100)
                            {
                                _temperature_decreament = 3;
                                FlashTemperature_item.StopFlow();//取消闪烁
                                _replace_count2 = is_reaction_over ? _replace_count2 = 1 : -1;//反正完成后，开始降温后
                            }

                            SpTemperatureValue(_temperature_value);
                            LabelTemperature_item.Write(_temperature_value);
                        }
                        else
                        {
                            if (!Isinner) ValveCold_item.Close();
                            FlowInCold_item.StopFlow();
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });
                #endregion

                #region 出料阀 控制
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)//true
                    {
                        if (ValveDischarge_State)
                        {
                            if (!Isinner) ValveDischarge_item.Open();
                            FlowDischarge_item.StartFlow();
                        }
                        else
                        {
                            if (!Isinner) ValveDischarge_item.Close();
                            FlowDischarge_item.StopFlow();
                        }
                        Thread.Sleep(_sleepslow);
                    }
                });
                #endregion
            });
        }

        
        #endregion
        

        #region 写串口数据
        private void SpFlowValue(double value)
        {
            WriteFlowValue(_llcmd, 1, value);
            WriteAIvalue(_aicmd, 3, value);
        }

        void SpPressureValue(double value)
        {
            WritePressureValue(_ylcmd, value);
            WriteAIvalue(_aicmd, 1, value);
        }

        void SpTemperatureValue(double value)
        {
            WriteTemperatureValue(_wdcmd, value);
            WriteAIvalue(_aicmd, 2, value);
        }

        /// <summary>
        /// 设置液位值
        /// </summary>
        /// <param name="index">液位索引：1或者2</param>
        /// <param name="value"></param>
        /// <param name="ywindex">液位index 命令id</param>

        void SPYWValue(int index, double value)
        {
            if (Isinner) return;
            int ywindex = 2;
            double write_value = 0;
            switch (index)
            {
                case 1:
                    ywindex = 2;
                    write_value = 255 * value / 600;
                    break;
                case 2:
                    ywindex = 1;
                    write_value = 255 * value / 400;
                    break;
            }
            write_value = Math.Round(write_value, 0);

            string cmdvalue = write_value.ToString().PadLeft(3, '0');
            string cmd = string.Format(_ywcmd, ywindex, cmdvalue);
            lock (this)
            {
                Thread.Sleep(150);
                if(_serialPort.IsOpen)
                _serialPort.Write(cmd);
            }
        }

        #endregion

        #region 操作继电器
        /// <summary>
        /// 写继电器
        /// </summary>
        /// <param name="number">0-128， </param>
        void SPWriteTool(int number)
        {
            if (Isinner) return;
            WriteTool(_relaycmd, number);
        }

        void CloseAllRelay()
        {
            SPWriteTool(0);
        }

        /// <summary>
        /// 开始预警
        /// </summary>
        /// <param name="type">预警类型</param>
        void BeginWarning(WarningType type)
        {
            if (!_warningtypes.Contains(type))
            {
                FlashWarning_item.Open();
                _warningtypes.Add(type);
                _relayCommand = _relayCommand | _warning1number;
                _relayCommand = _relayCommand | _warning2number;
                SPWriteTool(_relayCommand);
            }
        }

        void StopWarning(WarningType type)
        {
            if (_warningtypes.Contains(type))
            {
                FlashWarning_item.Close();
                _relayCommand = (~_warning1number) & _relayCommand;
                _relayCommand = (~_warning2number) & _relayCommand;
                SPWriteTool(_relayCommand);
                _warningtypes.Remove(type);
            }
        }

        /// <summary>
        /// 开始泄漏
        /// </summary>
        void BeginLeak()
        {
            FlashWarning_item.Open();
            _relayCommand = _relayCommand | _fognumber;
            SPWriteTool(_relayCommand);
            Thread.Sleep(500);
            StopLeak();
        }
        /// <summary>
        /// 停止泄漏
        /// </summary>
        void StopLeak()
        {
            FlashWarning_item.Close();
            _relayCommand = _relayCommand & (~_fognumber);
            SPWriteTool(_relayCommand);
        }

        void BeginStir()
        {
            _relayCommand = _relayCommand | _stirnumber;
            SPWriteTool(_relayCommand);
        }

        void StopStir()
        {
            _relayCommand = _relayCommand & (~_stirnumber);
            SPWriteTool(_relayCommand);
        }
        #endregion

        #region 枚举定义
        /// <summary>
        /// 组态字段-字符串
        /// </summary>
        enum FiledName
        {
            /// <summary>
            /// 硝基物液位
            /// </summary>
            LabelNitro,
            /// <summary>
            /// 甲醇液位
            /// </summary>
            LabelMethanol, LabelTemperature, LabelPresssure, LabelFlow, LabelOxygen,
            
            //阀门
            ValveInNitro, ValveInMethanol, ValveOutNitro, ValveOutMethanol, ValveInN2, ValveEmpty, ValveCold, ValveInHot, ValveInH2, ValveDischarge, ValveAutoEmpty, ValveICutoff, ValveOverNitro, ValveOverMethanol,
            ValveStir,ValveStop,
            //管道
            FlowInNitro,FlowInMethanol,FlowOutMethanol,FlowOutNitro,FlowOutEmpty,FlowInCold,FlowInHot,FlowInH2,FlowDischarge,FlowInN2,
            //flash
            FlashPressure, FlashTemperature,
            /// <summary>
            /// 催化剂
            /// </summary>
            FlashCatalyst,
            /// <summary>
            /// 搅拌釜
            /// </summary>
            FlashStir, FlashWarning
        }
        /// <summary>
        /// 状态名称
        /// </summary>
        enum StatusName
        {
            硝基物进料阀, 甲醇进料阀, 硝基物放料阀, 甲醇放料阀,
            硝基物液位, 甲醇液位, 温度, 压力, 流量, 自动放空阀, 冷水阀, 蒸汽阀, 氢气阀, 紧急切断阀, 放料阀, 搅拌阀,
            放空阀1, 氮气阀1, 放空阀2, 氮气阀2, 放空阀3, 氮气阀3, 放空阀4, 氮气阀4,
            急停按钮,

        }

        enum WarningType
        {
            甲醇液位过高,
            硝基物液位过高,
            通入甲醇过量,
            通入硝基物过量,
            氮气置换压力过高,
            温度异常,
            压力异常,
            泄漏,
            反应釜压力过高,
            温度过高,
        }

        #endregion
        
        #region 2015-11-18 添加分步考试计分
        private DateTime _leakAbnormalDate;

        private DateTime _pressureAbnormalData;

        private DateTime _temperatureAbnormalDate;
        private int _loopcount = 0;
        private void CheckAbnormalPoint()
        {
            var pressureAbnormal = base._points.SingleOrDefault(i => i.Description == "压力突变异常");
            var leakAbnormal = _points.SingleOrDefault(i => i.Description == "泄漏异常");
            var temperatureAbnormal = _points.SingleOrDefault(i => i.Description == "温度突变异常");
            //压力异常
            if ( pressureAbnormal.IsChecked)
            {
                Task.Run(
                    () =>
                    {
                        while (!_listenCts.IsCancellationRequested)
                        {
                            if (_loopcount == 240) //2分钟
                            {
                                this.BeginPressureAbnormal(pressureAbnormal.Value1);
                                break;
                            }
                            Thread.Sleep(_sleepfast);
                        }
                    });
            }
            //温度异常
            if (temperatureAbnormal.IsChecked)
            {
                Task.Run(
                    () =>
                    {
                        while (!_listenCts.IsCancellationRequested)
                        {
                            if (_loopcount == 240) //2分钟
                            {
                                this.BeginTempretureAbnormal(temperatureAbnormal.Value1);
                                break;
                            }
                            Thread.Sleep(_sleepfast);
                        }
                    });
            }
            //泄漏
            if ( leakAbnormal.IsChecked)
            {
                Task.Run(
                   () =>
                   {
                       while (!_listenCts.IsCancellationRequested)
                       {
                           if (_loopcount == 240) //2分钟
                           {
                               this.BeginLeakAbnormal();
                               break;
                           }
                           Thread.Sleep(_sleepfast);
                       }
                   });
            }

        }

        #region 压力异常 -处理方法，打开放空阀，关机紧急阀

        private void BeginPressureAbnormal(double limit)
        {
            _warningDicionary.Add(WarningType.压力异常, false);
            Task.Run(() =>
            {
                while (_pressure_value < limit)
                {
                    _pressure_value += 0.1;
                    LabelPressure_item.Write(_pressure_value);
                    SpPressureValue(_pressure_value);
                    Thread.Sleep(_sleepfast);
                }
                _pressureAbnormalData = DateTime.Now;
                BeginWarning(WarningType.压力异常);
                FlashPressure_item.StartFlow();
                HandlePressureAbnormal();
            });
        }
        private void HandlePressureAbnormal()
        {
            Parallel.Invoke(OpenAirValve, CloseH2Valve);

            StopWarning(WarningType.压力异常);
            _warningDicionary[WarningType.压力异常] = true;
        }

        private void OpenAirValve()
        {
            _pressure_decrement = 0.02; //加快降压
            //打开放空阀
            while (!_listenCts.IsCancellationRequested)
            {
                if (ValveEmpty_State)
                {
                    if (_pressure_value <= 0.01)
                    {
                        break;
                    }
                }
                Thread.Sleep(_sleepfast);
            }
        }

        private void CloseH2Valve()
        {
            while (!_listenCts.IsCancellationRequested)
            {
                if (!ValveICutoff_State)
                {
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
        }
        #endregion

        #region 泄漏异常-按下急停按钮、关闭蒸汽和氢气阀
        void BeginLeakAbnormal()
        {
            BeginWarning(WarningType.泄漏);
            BeginLeak();
            _warningDicionary.Add(WarningType.泄漏, false);
            _leakAbnormalDate = DateTime.Now;
            HandleLeakAbnormal();
        }

        private void HandleLeakAbnormal()
        {
            //打开急停按钮
            while (!_listenCts.IsCancellationRequested)
            {
                if (ValveStop_State)
                {
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
            //关闭氢气和蒸汽
            Parallel.Invoke(() =>
            {
                while (!_listenCts.IsCancellationRequested)
                {
                    if (!ValveInHot_State)
                    {
                        break;
                    }
                    Thread.Sleep(_sleepfast);
                }
            }, () =>
            {
                while (!_listenCts.IsCancellationRequested)//true
                {
                    if (!ValveInH2_State)
                    {
                        break;
                    }
                    Thread.Sleep(_sleepfast);
                }
            });

            StopWarning(WarningType.泄漏);
            _warningDicionary[WarningType.泄漏] = true;
        }

        #endregion

        #region 温度异常-处理方法：关闭蒸汽阀、打开冷水阀将至50度以下
        private void BeginTempretureAbnormal(double limit)
        {
            _warningDicionary.Add(WarningType.温度异常, false);
            Task.Run(() =>
            {
                while (_temperature_value < limit)
                {
                    _temperature_value += 5;
                    LabelTemperature_item.Write(_temperature_value);
                    SpTemperatureValue(_temperature_value);
                    Thread.Sleep(_sleepfast);
                }
                _temperatureAbnormalDate = DateTime.Now;
                BeginWarning(WarningType.温度异常);
                FlashTemperature_item.StartFlow();
                HandleTempretureAbnormal();
            });
        }
        private void HandleTempretureAbnormal()
        {
            Parallel.Invoke(CloseHotValve, OpenColdValve);
            StopWarning(WarningType.温度异常);
            _warningDicionary[WarningType.温度异常] = true;
        }

        private void OpenColdValve()
        {
            Task.Run(() =>
            {
                while (!_listenCts.IsCancellationRequested) //true
                {
                    if (_temperature_value <= 50)
                    {
                        break;
                    }
                    Thread.Sleep(_sleepslow);
                }
            });
        }

        private void CloseHotValve()
        {
            Task.Run(() =>
            {
                while (!_listenCts.IsCancellationRequested) //true
                {
                    if (!ValveInHot_State)
                    {
                        break;
                    }
                    Thread.Sleep(_sleepslow);
                }
            });
        }

        #endregion
      

        private void CalcFlowAndStep()
        {
            #region 计算
            //硝基物进料
            var nitro_in_item = _steps.Where(i => i.Status[StatusName.硝基物进料阀] == open).OrderByDescending(i => i.Status[StatusName.硝基物液位]).FirstOrDefault();

            //甲醇进料
            var methenol_in_item = _steps.Where(i => i.Status[StatusName.甲醇进料阀] == open).OrderByDescending(i => i.Status[StatusName.甲醇液位]).FirstOrDefault();

            //硝基物出料
            var nitro_out_item = _steps.Where(i => i.Status[StatusName.硝基物放料阀] == open)
                .OrderBy(i => i.Status[StatusName.硝基物液位]).OrderByDescending(i => i.Date).FirstOrDefault();

            //甲醇出料
            var methenol_out_item = _steps.Where(i => i.Status[StatusName.甲醇放料阀] == open)
                .OrderBy(i => i.Status[StatusName.甲醇液位]).OrderByDescending(i => i.Date).FirstOrDefault();

            //氮气置换
            //氮气1
            var n2_pressure_item1 = _steps.Where(i => i.Status[StatusName.氮气阀1] == open)
                .OrderByDescending(i => i.Status[StatusName.压力]).OrderByDescending(i => i.Date).FirstOrDefault();

            //放空1 
            var o2_pressure_item1 = n2_pressure_item1 != null ? _steps.Where(i => i.Status[StatusName.放空阀1] == open && i.Date > n2_pressure_item1.Date)
                .OrderBy(i => i.Status[StatusName.压力]).OrderByDescending(i => i.Date).FirstOrDefault() : null;

            //氮气2
            var n2_pressure_item2 = o2_pressure_item1 != null ? _steps.Where(i => i.Status[StatusName.氮气阀2] == open && i.Date > o2_pressure_item1.Date)
                .OrderByDescending(i => i.Status[StatusName.压力]).OrderByDescending(i => i.Date).FirstOrDefault() : null;

            //放空2 
            var o2_pressure_item2 = n2_pressure_item2 != null ? _steps.Where(i => i.Status[StatusName.放空阀2] == open && i.Date > n2_pressure_item2.Date)
                .OrderBy(i => i.Status[StatusName.压力]).OrderByDescending(i => i.Date).FirstOrDefault() : null;

            //加氢
            var h2_item = _steps.Where(i => i.Status[StatusName.氢气阀] == open)
                 .OrderByDescending(i => i.Status[StatusName.压力]).ThenByDescending(i => i.Date).FirstOrDefault();

            //加热
            var hot_temperature_item = _steps.Where(i => i.Status[StatusName.蒸汽阀] == open)
                .OrderByDescending(i => i.Status[StatusName.温度]).ThenByDescending(i => i.Date).FirstOrDefault();

            //降温
            var cold_temperature_item = _steps.Where(i => i.Status[StatusName.冷水阀] == open)
                .OrderBy(i => i.Status[StatusName.温度]).OrderByDescending(i => i.Date).FirstOrDefault();

            //关搅拌 一直开 直到反应结束,最后加
            var stir_open_first = _steps.FirstOrDefault(i => i.Status[StatusName.搅拌阀] == open);//首次打开的时间       

            //放料
            var discharge_open_item = _steps.Where(i => i.Status[StatusName.放料阀] == open).OrderBy(i => i.Date).FirstOrDefault();

            #endregion
            #region 添加步骤
            _dicSteps.Clear();
            

            #region 进料出料

            //  硝基物进料
            InsertRangeValueStep("硝基物进料", ValueName.液位,nitro_in_item, StatusName.硝基物液位);

            // 甲醇进料
            InsertRangeValueStep("甲醇进料", ValueName.液位, methenol_in_item, StatusName.甲醇液位);

            #region 硝基物出料
            var nitro_out_step = base._points.SingleOrDefault(i => i.Description == "硝基物出料");
            if (nitro_out_step.IsChecked)
            {
                if (nitro_out_item != null)
                {
                    double nitro_out_minvalue = nitro_out_item.Status[StatusName.硝基物液位];
                    double nitro_in_maxvalue = nitro_in_item.Status[StatusName.硝基物液位];//340

                    var diff_nitro = nitro_in_maxvalue - nitro_out_minvalue;
                    if (diff_nitro >= nitro_out_step.Value1)
                    {
                        string tempformart = string.Format("{0}:液位差{1}{2}{3}", nitro_out_step.Description, diff_nitro, nitro_out_step.Operator, nitro_out_step.Value1);
                        _dicSteps.Add("硝基物出料", new StepDescription() { Description = tempformart, IsRight = true });
                    }
                    else
                    {
                        string tempformart = string.Format("{0}:液位差{1},{2}{3}", nitro_out_step.Description, diff_nitro, "低于", nitro_out_step.Value1);
                        _dicSteps.Add("硝基物出料", new StepDescription() { Description = tempformart, IsRight = false });
                    }
                }
                else
                {
                    _dicSteps.Add("硝基物出料", new StepDescription() { Description = nitro_out_step.Description + "：无操作", IsRight = false });
                }
            }
            #endregion

            #region 甲醇出料
            var methenol_out_step = base._points.SingleOrDefault(i => i.Description == "甲醇出料");
            if (methenol_out_step.IsChecked)
            {
                if (methenol_out_item != null)
                {
                    var methenol_out_minvalue = methenol_out_item.Status[StatusName.甲醇液位];
                    var methenol_in_maxvalue = methenol_in_item.Status[StatusName.甲醇液位];//240

                    var diff_methenol = methenol_in_maxvalue - methenol_out_minvalue;
                    if (diff_methenol >= methenol_out_step.Value1)
                    {
                        string tempformart = string.Format("{0}:液位差：{1} {2} {3}", methenol_out_step.Description, diff_methenol, methenol_out_step.Operator, methenol_out_step.Value1);
                        _dicSteps.Add("甲醇出料", new StepDescription() { Description = tempformart, IsRight = true });
                    }
                    else
                    {
                        string tempformart = string.Format("{0}:液位差：{1},{2} {3}", methenol_out_step.Description, diff_methenol, "低于", methenol_out_step.Value1);
                        _dicSteps.Add("甲醇出料", new StepDescription() { Description = tempformart, IsRight = false });
                    }
                }
                else
                {
                    _dicSteps.Add("甲醇出料", new StepDescription() { Description = methenol_out_step.Description + "：无操作", IsRight = false });
                }
            }
            #endregion
            #endregion

            #region 氮气置换
            var n2_replace_step = base._points.SingleOrDefault(i => i.Description == "氮气置换");
            if (n2_replace_step.IsChecked)
            {
                if (n2_pressure_item1 != null)
                {
                    var pressure_value = n2_pressure_item1.Status[StatusName.压力];
                    if (pressure_value >= 0.3)
                    {
                        string temp = string.Format("反应釜压力:{0}MPa,高于0.3Mpa,压力过高", pressure_value);
                        _dicSteps.Add("氮气置换-充氮气", new StepDescription() { Description = temp, IsRight = false });
                    }
                    else if (pressure_value >= 0.2 && pressure_value < 0.3)
                    {
                        string temp = string.Format("反应釜压力:{0}MPa,在0.2Mpa-0.3Mpa范围内", pressure_value);
                        _dicSteps.Add("氮气置换-充氮气", new StepDescription() { Description = temp, IsRight = true });
                    }
                    else
                    {
                        string temp = string.Format("反应釜压力:{0}MPa,低于0.2Mpa,压力不足", pressure_value);
                        _dicSteps.Add("氮气置换-充氮气", new StepDescription() { Description = temp, IsRight = false });
                    }
                }
                else
                {
                    _dicSteps.Add("氮气置换-充氮气", new StepDescription() { Description = "氮气置换-充氮气，无操作", IsRight = false });
                }

                if (o2_pressure_item1 != null)
                {
                    var pressure_value = o2_pressure_item1.Status[StatusName.压力];
                    if (pressure_value <= 0.01)
                    {
                        string temp = string.Format("打开放空阀，降压至{0}MPa", pressure_value);
                        _dicSteps.Add("氮气置换-放空", new StepDescription() { Description = temp, IsRight = true });
                    }
                    else
                    {
                        string temp = string.Format("打开放空阀，反应釜压力{0}MPa，未降到0.01MPa以下", pressure_value);
                        _dicSteps.Add("氮气置换-放空", new StepDescription() { Description = temp, IsRight = false });
                    }
                }
                else
                {
                    _dicSteps.Add("氮气置换-放空", new StepDescription() { Description = "氮气置换-放空，无操作", IsRight = false });
                }
            }
            #endregion

            // 通入蒸汽
            InsertRangeValueStep("通入蒸汽", ValueName.温度, hot_temperature_item, StatusName.温度);

            // 通入氢气
            InsertRangeValueStep("通入氢气", ValueName.压力, h2_item, StatusName.压力);
            
           // 通入冷水
            InsertRangeValueStep("通入冷水", ValueName.温度, cold_temperature_item, StatusName.温度);

            // 开搅拌 / 放料
            InsertActionStep("打开搅拌", stir_open_first);
            InsertActionStep("放料", discharge_open_item);       



            //异常处理
            #region 压力异常
            var pressure_abnormal_point = base._points.SingleOrDefault(i => i.Description == "压力突变异常");
            if (pressure_abnormal_point.IsChecked)
            {
                var openair_item = _steps.Where(i => i.Status[StatusName.压力] <= 0.01 && i.Date > _pressureAbnormalData)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closeh2_item = _steps.Where(i => i.Status[StatusName.氢气阀] == close && i.Date > _pressureAbnormalData)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (openair_item != null && closeh2_item != null)
                {
                    _dicSteps.Add("处理压力突变异常", new StepDescription() { Description = pressure_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理压力突变异常", new StepDescription() { Description = pressure_abnormal_point.Description + "：未完成", IsRight = false });
                }
            } 
            #endregion

            #region 温度异常
            var temperature_abnormal_point = base._points.SingleOrDefault(i => i.Description == "温度突变异常");
            if (temperature_abnormal_point.IsChecked)
            {
                var openair_item = _steps.Where(i => i.Status[StatusName.温度] <= 50 && i.Date > _temperatureAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closehot_item = _steps.Where(i => i.Status[StatusName.蒸汽阀] == close && i.Date > _temperatureAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (openair_item != null && closehot_item != null)
                {
                    _dicSteps.Add("处理温度突变异常", new StepDescription() { Description = temperature_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理温度突变异常", new StepDescription() { Description = temperature_abnormal_point.Description + "：未完成", IsRight = false });
                }
            }
            #endregion

            #region 泄漏异常
            var leak_abnormal_point = base._points.SingleOrDefault(i => i.Description == "泄漏异常");
            if (leak_abnormal_point.IsChecked)
            {
                var closeHot_item = _steps.Where(i => i.Status[StatusName.蒸汽阀] ==close && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closeH2_item = _steps.Where(i => i.Status[StatusName.氢气阀] == close && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var stop_item = _steps.Where(i => i.Status[StatusName.急停按钮] == open && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (closeHot_item != null && closeH2_item != null && stop_item!=null)
                {
                    _dicSteps.Add("处理泄漏异常", new StepDescription() { Description = leak_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理泄漏异常", new StepDescription() { Description = leak_abnormal_point.Description + "：未完成", IsRight = false });
                }
            }
            #endregion

            #endregion
        }
        #endregion
    }
}

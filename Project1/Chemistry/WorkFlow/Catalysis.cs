﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCAutomation;
using System.IO.Ports;
using Chemistry.Models;
using System.Threading;

namespace Chemistry.WorkFlow
{
    /// <summary>
    /// 催化裂解工艺
    /// </summary>
    public class Catalysis: FlowBaseAll
    {
        #region 字段
        private int _sleepfast = 500;

        private static int _relay_command;
        static bool _isgetoverstate1 = false;
        static bool _isgetoverstate2 = false;
        List<Step> _steps = new List<Step>();

        //组态数据
        static double _settlingChamber_pressure_value;
        static double _riserReactor_temp_value;
        static double _charringTank_pressure_value;
        static double _regenerator_preesure_value;
        static double _regenerator_temp_value;
        static double _flow_air_value;
        static double _exCooler_level_value;

        //组态数据范围
        static double _settlingChamber_pressure_max;
        static double _settlingChamber_pressure_min;
        static double _riserReactor_temp_max;
        static double _riserReactor_temp_min;
        static double _charringTank_pressure_max;
        static double _charringTank_pressure_min;
        static double _regenerator_preesure_max;
        static double _regenerator_preesure_min;
        static double _regenerator_temp_max;
        static double _regenerator_temp_mix;
        static double _flow_air_max;
        static double _flow_air_min;
        static double _exCooler_level_max;
        static double _exCooler_level_min;

        //增减量
        static double _settlingChamber_pressure_increment;
        static double _settlingChamber_pressure_decrement;
        static double _riserReactor_temp_increment;
        static double _riserreactor_temp_decrement;
        static double _charringTank_pressure_increment;
        static double _charringTank_pressure_decrement;
        static double _regenerator_preesure_increment;
        static double _regenerator_preesure_decrement;
        static double _regenerator_temp_increment;
        static double _regenerator_temp_decrement;
        static double _flow_air_increment;
        static double _flow_air_decrement;
        static double _exCooler_level_increment;
        static double _exCooler_level_decrement;










        //命令
        private Tools.Tool.FlowCommand _flowcommand;
        string _switch1 = string.Empty;
        string _switch2 = string.Empty;
        string _prefix1 = string.Empty;
        string _prefix2 = string.Empty;
        string _llcmd = string.Empty;
        string _ywcmd = string.Empty;
        string _ylcmd = string.Empty;
        string _wdcmd = string.Empty;
        string _aicmd = string.Empty;
        string _relaycmd = string.Empty;
        int _warning1number = 0;
        int _warning2number = 0;
        int _fognumber = 0;
        int _stirnumber;
        //开关
        int open = 1; //0-48
        int close = 0; //1=49 
        #endregion

        #region 组态开关状态
        private static bool ValveInStrippedVapor_state;
        private static bool ValveInCatalyst_state;
        private static bool ValveInMixedOil_state;
        private static bool ValveInSteam_state;
        private static bool ValveStrippedCatalyst_state;
        private static bool ValveInAir_state;
        private static bool ValveRiserReactorCatalyst_state;
        private static bool ValveCharringTankCatalyst_state;
        private static bool ValveExternalCoolerCatalyst_state;
        private static bool ValveRegCatalyst_state;
        private static bool ValveInFuelOil_state;
        private static bool ValveInCold_state;
        private static bool ValveDraftFan_state;
        #endregion

        #region OPCItem
        //阀门
        OPCItem ValveInStrippedVapor_item; //汽提蒸汽阀门
        OPCItem ValveInCatalyst_item;//催化剂阀门
        OPCItem ValveInMixedOil_item;//混合油阀门
        OPCItem ValveInSteam_item;//水蒸气阀门
        OPCItem ValveStrippedCatalyst_item; //汽提催化剂阀门
        OPCItem ValveInAir_item;//空气阀门
        OPCItem ValveRiserReactorCatalyst_item;// 再生催化剂流入提升反应器阀门
        OPCItem ValveCharringTankCatalyst_item;//再生催化剂流入烧焦罐阀门
        OPCItem ValveExternalCoolerCatalyst_item;//再生催化剂流入外取热器阀门
        OPCItem ValveRegCatalyst_item;// 再生催化剂从外取热器流入烧焦罐阀门
        OPCItem ValveInFuelOil_item;//开工燃料油阀门
        OPCItem ValveInCold_item;//冷却水阀门
        OPCItem ValveDraftFan_item;//引风机阀门


        //管道
        OPCItem FlowStrippedVapor_item; //汽提蒸汽管道
        OPCItem FlowInCatalyst_item;//催化剂原料管道
        OPCItem FlowInMixedOil_item;//混合油原料管道
        OPCItem FlowStrippedCatalyst_item;//汽提催化剂管道
        OPCItem FlowInAir_item;//空气管道
        OPCItem FlowRiserReactorCatalyst_item;//再生催化剂流入提升反应器管道
        OPCItem FlowCharringTankCatalyst_item;//再生催化剂流入烧焦罐管道
        OPCItem FlowExternalCoolerCatalyst_item;//再生催化剂流入外取热管道
        OPCItem FlowRegCatalyst_item;//再生催化剂从外取热器流入烧焦器
        OPCItem FlowInSteam_item; //水蒸汽
        OPCItem FlowInFuelOil_item;//燃料油管道
        OPCItem FlowInCold_item;//冷却水管道
        OPCItem FlowOutAir_item;//引风机管道
        //标签
        OPCItem LabelPressureSettlingChamber_item;//沉降室压力
        OPCItem LabelPressureCharringTank_item;//烧焦罐压力
        OPCItem LabelTempRiserReactor_item;//提升管反应器温度
        OPCItem LabelFlowAir_item;
        OPCItem LabelTempRegenerator_item;//再生器温度
        OPCItem LabelPressureRegenerator_item;//再生器压力
        OPCItem LabelLevel_item;//外取热器液位
        //闪烁
        OPCItem FlashPressureSettlingChamber_item;
        OPCItem FlashlPressureCharringTank_item;
        OPCItem FlashTempRiserReactor_item;
        OPCItem FlashFlowAir_item;
        OPCItem FlashTempRegenerator_item;
        OPCItem FlashPressureRegenerator_item;
        OPCItem FlashPressurExCooler_item;




        #endregion

        #region 初始化
        public Catalysis(OPCServer opcserver, SerialPort port, bool isinner)
        {
            _opcServer = opcserver;
            _serialPort = port;
            Isinner = isinner;

            InitFiled();
            InitOPC();
            InitOPCItem();
          //  InitServerData();
            InitSignal();

        }
        private void InitFiled()
        {
            _settlingChamber_pressure_value = 0;
            _riserReactor_temp_value = 22;
            _charringTank_pressure_value = 0;
            _regenerator_preesure_value = 0.1;
            _regenerator_temp_value = 22;
            _flow_air_value = 0;
            _exCooler_level_value = 0;







            _settlingChamber_pressure_increment =0.01;
            _settlingChamber_pressure_decrement=0.01;
            _riserReactor_temp_increment = 10;
            _riserreactor_temp_decrement = 5;
            _charringTank_pressure_increment = 0.01;
            _charringTank_pressure_decrement = 0.01;
            _charringTank_pressure_increment = 0.01;
            _charringTank_pressure_decrement = 0.01;
            _regenerator_preesure_increment=0.01;
            _riserreactor_temp_decrement = 0.01;
            _regenerator_temp_increment = 10;
            _riserreactor_temp_decrement = 5;
            _flow_air_increment = 5;
            _flow_air_decrement = 5;
            _exCooler_level_increment =5;
            _exCooler_level_decrement =1;


            _settlingChamber_pressure_max = 0.2;
            _settlingChamber_pressure_min = 0.13;
            _riserReactor_temp_max = 530;
            _riserReactor_temp_min = 480;
            _regenerator_preesure_max = 0.6;
            _regenerator_preesure_min = 0.1;
            _regenerator_temp_max = 730;
            _regenerator_temp_mix = 600;
            _flow_air_max = 1350;
            _flow_air_min = 1280;
            _exCooler_level_max = 60;
            _exCooler_level_min = 40;
            _charringTank_pressure_max = 0.6;
            _charringTank_pressure_min = 0.4;





        }
        
        private void InitOPCItem()
        {
            ValveInStrippedVapor_item = GetItem(FiledName.ValveInStrippedVapor); ValveInStrippedVapor_item.Close();
            ValveInCatalyst_item = GetItem(FiledName.ValveInCatalyst);ValveInCatalyst_item.Close();
            ValveInSteam_item = GetItem(FiledName.ValveInSteam);ValveInCatalyst_item.Close();
            ValveStrippedCatalyst_item = GetItem(FiledName.ValveStrippedCatalyst);ValveStrippedCatalyst_item.Close();
            ValveInAir_item = GetItem(FiledName.ValveInAir);ValveInAir_item.Close();
            ValveRiserReactorCatalyst_item = GetItem(FiledName.ValveRiserReactorCatalyst);ValveRiserReactorCatalyst_item.Close();
            ValveCharringTankCatalyst_item = GetItem(FiledName.ValveCharringTankCatalyst);ValveCharringTankCatalyst_item.Close();
            ValveExternalCoolerCatalyst_item = GetItem(FiledName.ValveExternalCoolerCatalyst);ValveExternalCoolerCatalyst_item.Close();
            ValveRegCatalyst_item = GetItem(FiledName.ValveRegCatalyst);ValveRegCatalyst_item.Close();
            ValveDraftFan_item = GetItem(FiledName.ValveDraftFan); ValveDraftFan_item.Close();
            ValveInFuelOil_item = GetItem(FiledName.ValveInFuelOil);ValveInFuelOil_item.Close();
            ValveInCold_item = GetItem(FiledName.ValveInCold);ValveInCold_item.Close();
            ValveDraftFan_item = GetItem(FiledName.ValveDraftFan);ValveDraftFan_item.Close();

            FlowStrippedVapor_item = GetItem(FiledName.FlowStrippedVapor); FlowStrippedVapor_item.Close();
            FlowInCatalyst_item = GetItem(FiledName.FlowInCatalyst);FlowInCatalyst_item.Close();
            FlowInMixedOil_item = GetItem(FiledName.FlowInMixedOil);FlowInMixedOil_item.Close();
            FlowStrippedCatalyst_item = GetItem(FiledName.FlowStrippedCatalyst);FlowInCatalyst_item.Close();
            FlowInAir_item = GetItem(FiledName.FlowInAir);FlowInAir_item.Close();
            FlowRiserReactorCatalyst_item = GetItem(FiledName.FlowRiserReactorCatalyst);FlowRiserReactorCatalyst_item.Close();
            FlowCharringTankCatalyst_item = GetItem(FiledName.FlowCharringTankCatalyst);FlowCharringTankCatalyst_item.Close();
            FlowExternalCoolerCatalyst_item = GetItem(FiledName.FlowExternalCoolerCatalyst);FlowExternalCoolerCatalyst_item.Close();
            FlowRegCatalyst_item = GetItem(FiledName.FlowRegCatalyst);FlowRegCatalyst_item.Close();
            FlowInSteam_item = GetItem(FiledName.FlowInSteam); FlowInSteam_item.Close();
            FlowOutAir_item= GetItem(FiledName.FlowOutAir); FlowOutAir_item.Close();
            FlowInFuelOil_item = GetItem(FiledName.FlowInFuelOil);FlowInFuelOil_item.Close();
            FlowInCold_item = GetItem(FiledName.FlowInCold);FlowInCold_item.Close();

            LabelPressureSettlingChamber_item = GetItem(FiledName.LabelPressureSettlingChamber);LabelPressureSettlingChamber_item.Write(_settlingChamber_pressure_value);
            LabelPressureCharringTank_item = GetItem(FiledName.LabelPressureCharringTank);LabelPressureCharringTank_item.Write(_charringTank_pressure_value);
            LabelTempRiserReactor_item = GetItem(FiledName.LabelTempRiserReactor);LabelTempRiserReactor_item.Write(_riserReactor_temp_value);
            LabelFlowAir_item = GetItem(FiledName.LabelFlowAir);LabelFlowAir_item.Write(_flow_air_value);
            LabelTempRegenerator_item = GetItem(FiledName.LabelTempRegenerator);LabelTempRegenerator_item.Write(_regenerator_temp_value);
            LabelPressureRegenerator_item = GetItem(FiledName.LabelPressureRegenerator); LabelPressureRegenerator_item.Write(_regenerator_preesure_value);
            LabelLevel_item = GetItem(FiledName.LabelLevel);LabelLevel_item.Write(_exCooler_level_value);

            FlashPressureSettlingChamber_item = GetItem(FiledName.FlashPressureSettlingChamber); FlashPressureSettlingChamber_item.Close();
            FlashlPressureCharringTank_item = GetItem(FiledName.FlashlPressureCharringTank); FlashlPressureCharringTank_item.Close();
            FlashTempRiserReactor_item = GetItem(FiledName.FlashTempRiserReactor);FlashTempRiserReactor_item.Close();
            FlashFlowAir_item = GetItem(FiledName.FlashFlowAir);FlashFlowAir_item.Close();
            FlashTempRegenerator_item = GetItem(FiledName.FlashTempRegenerator);FlashTempRegenerator_item.Close();
            FlashPressureRegenerator_item = GetItem(FiledName.FlashPressureRegenerator);FlashPressureRegenerator_item.Close();
            FlashPressurExCooler_item = GetItem(FiledName.FlashPressurExCooler);FlashPressurExCooler_item.Close();
        }


        private async Task InitServerData()
        {
            var abnormalRusult = await GetAbnormalModel(GetType().Name);
            if (abnormalRusult != null && abnormalRusult.status == 0)
                _abnormals = abnormalRusult.abnormals;
            var d = await GetDetailModel(GetType().Name);
            if (d != null && d.status == 0)
                _details = d.details;
        }

        private void InitSignal()
        {
            if (Isinner)
            {
                open = 1;
                close = 0;
                ListenSignal();

            }
            else
            {
                InitFlowCommandData();
                _serialPort.DataReceived += _serialPort_DataReceived;
                InitOpcItemWithSerialPort();
                ListenSerialPortAndAbnormalData();
                open = 48;
                close = 49;
            }

        }


        #region InitSignal
        private void ListenSignal()
        {
            Task t = new Task(() => 
            {
                while(!_listenCts.IsCancellationRequested)
                {
                    int valveInStrippedVapor = Convert.ToInt32(ReadItem(FiledName.ValveInStrippedVapor));
                    int valveInCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveInCatalyst));
                    int valveInMixedOil = Convert.ToInt32(ReadItem(FiledName.ValveInMixedOil));
                    int valveInSteam = Convert.ToInt32(ReadItem(FiledName.ValveInSteam));
                    int valveStrippedCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveStrippedCatalyst));
                    int valveInAir = Convert.ToInt32(ReadItem(FiledName.ValveInAir));
                    int valveRiserReactorCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveRiserReactorCatalyst));
                    int valveCharringTankCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveCharringTankCatalyst));
                    int valveExternalCoolerCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveExternalCoolerCatalyst));
                    int valveRegCatalyst = Convert.ToInt32(ReadItem(FiledName.ValveRegCatalyst));
                    int valveInFuelOil = Convert.ToInt32(ReadItem(FiledName.ValveInFuelOil));
                    int valveInCold = Convert.ToInt32(ReadItem(FiledName.ValveInCold));
                    int valveDraftFan = Convert.ToInt32(ReadItem(FiledName.ValveDraftFan));

                    ValveInStrippedVapor_state = valveInStrippedVapor == open;
                    ValveInCatalyst_state=valveInCatalyst==open;
                    ValveInMixedOil_state=valveInMixedOil==open;
                    ValveInSteam_state=valveInSteam==open;
                    ValveStrippedCatalyst_state=valveStrippedCatalyst==open;
                    ValveInAir_state=valveInAir==open;
                    ValveRiserReactorCatalyst_state=valveRiserReactorCatalyst==open;
                    ValveCharringTankCatalyst_state=valveCharringTankCatalyst==open;
                    ValveExternalCoolerCatalyst_state=valveExternalCoolerCatalyst==open;
                    ValveRegCatalyst_state=valveRiserReactorCatalyst==open;
                    ValveInFuelOil_state = valveInFuelOil == open;
                    ValveInCold_state = valveInCold == open;
                    ValveDraftFan_state = valveDraftFan == open;

                    DateTime dnow = DateTime.Now;
                    Step step = new Step() {
                       Status =
                        {
                            {StatusName.汽提蒸汽阀门,valveInStrippedVapor },
                            {StatusName.催化剂阀门,valveInCatalyst },
                            {StatusName.混合油阀门,valveInMixedOil},
                            {StatusName.水蒸气阀门,valveInSteam },
                            {StatusName.空气阀门,valveInAir },
                            {StatusName.再生催化剂流入提升反应器阀门,valveRiserReactorCatalyst},
                            {StatusName.再生催化剂流入烧焦罐阀门,valveCharringTankCatalyst },
                            {StatusName.再生催化剂流入外取热器阀门,valveExternalCoolerCatalyst},
                            {StatusName.再生催化剂从外取热器流入烧焦罐阀门,valveRegCatalyst },
                            {StatusName.沉降室压力,_settlingChamber_pressure_value},
                            {StatusName.提升管反应器温度,_riserReactor_temp_value },
                            {StatusName.再生器压力,_regenerator_preesure_value },
                            {StatusName.再生器温度,_regenerator_temp_value},
                            {StatusName.烧焦罐压力,_charringTank_pressure_value},
                            {StatusName.外取热器液位,_exCooler_level_value },
                            { StatusName.主风机流量,_flow_air_value},
                            { StatusName.燃料油阀门,valveInFuelOil},
                            { StatusName.冷却水阀门,valveInCold},
                    

                        },
                       Date=dnow,

                    };
                    _steps.Add(step);
                    Thread.Sleep(_sleepfast);
                }


            });
           
            t.Start();
        }

        private void InitFlowCommandData()
        {
            _flowcommand = Tools.Tool.GetFlowCommand(this.GetType().Name);
            _switch1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch1").Value;
            _switch2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch2").Value;
            _prefix1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix1").Value;
            _prefix2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix2").Value;
            _llcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "llcmd").Value;
            _ywcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ywcmd").Value;
            _ylcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ylcmd").Value;
            _wdcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "wdcmd").Value;
            _aicmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "aicmd").Value;
            _relaycmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "relaycmd").Value;
            _warning1number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning1").Value);
            _warning2number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning2").Value);
            _fognumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "fog").Value);
            _stirnumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "stir").Value);
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

        }

        private void InitOpcItemWithSerialPort()
        {

        }

        private void ListenSerialPortAndAbnormalData()
        {

        }
        #endregion
        #endregion

        #region 重写公共方法

        public override void Begin()
        {
            ListenOPCRelation();
        }
        public override async Task<string> CheckDefault()
        {
            StringBuilder builder = new StringBuilder();
            await InitServerData();
            if (_details == null || _abnormals == null)
                builder.AppendLine("可能由于网络原因，导致获取数据失败，请点击\"确定\"按钮 尝试重新获取");
            return builder.ToString();
        }
        public override void Over()
        {
            this._listenCts.Cancel();
            CalcFlowAndStep();
            Score = CalcScore();
            Finish();

        }

        private void ListenOPCRelation()
        {
            ThreadPool.QueueUserWorkItem(o=>
            {
                #region 反应系统

                #region  催化剂经再生器高温处理，开工炉打开

                #region 燃料油进入，开工
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveInFuelOil_state)
                        {
                            if (!Isinner)
                                ValveInFuelOil_item.Open();
                            FlowInFuelOil_item.StartFlow();
                            //烧焦罐压力范围[0.3-0.7]  实际[0.4-0.6]
                            _charringTank_pressure_increment = _charringTank_pressure_value < 0.3 ? 0.01 : 0.005;
                            _charringTank_pressure_increment = _charringTank_pressure_value > 0.7 ? 0 : 0.005;
                            _charringTank_pressure_value += _charringTank_pressure_increment;
                            LabelPressureCharringTank_item.Write(_charringTank_pressure_value);
                           //再生器温度[450-600] 实际[500-550]

                            _regenerator_temp_increment = _regenerator_temp_value < 450 ? 8 : 2;
                            _regenerator_temp_increment = _regenerator_temp_value > 550 ? 0 : 2;
                            _regenerator_temp_value += _regenerator_temp_increment;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveInFuelOil_item.Close();
                            FlowInFuelOil_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion
                #region 空气进入,助燃，
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInAir_state)
                        {
                            if (!Isinner)
                                ValveInAir_item.Open();
                            FlowInAir_item.StartFlow();
                            //主风机流量[1000 - 1400] 实际[1280 - 1350]
                            //主风机流量[300-600]  实际[1280-1350]*(30%-40%)=[380-550]
                            _flow_air_increment = _flow_air_value < 300 ? 10 : 5;
                            _flow_air_increment = _flow_air_value > 600 ? 0 : 5;
                            _flow_air_value += _flow_air_increment;
                            LabelFlowAir_item.Write(_flow_air_value);
                            //再生器温度[450-600] 实际[500-550]
                            _regenerator_temp_increment = _regenerator_temp_value < 450 ? 5 : 2;
                            _regenerator_temp_increment = _regenerator_temp_value > 550 ? 0 : 2;
                            _regenerator_temp_value += _regenerator_temp_increment;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveInAir_item.Close();
                            FlowInAir_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);

                    }
                });
                #endregion
                #region 冷却水进入外取热器
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveInCold_state)
                        {
                            if (!Isinner)
                                ValveInCold_item.Open();
                            FlowInCold_item.StartFlow();
                            //外取热器液位[40%-60%]
                            _exCooler_level_increment = _exCooler_level_value < 600 ? 10 : 0;
                            _exCooler_level_value += _exCooler_level_increment;
                            LabelLevel_item.Write(_exCooler_level_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveInCold_item.Close();
                            FlowInCold_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 催化剂进入再生器 
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveInCatalyst_state)
                        {
                            if (!Isinner)
                                ValveInCatalyst_item.Open();
                            FlowInCatalyst_item.StartFlow();
                            //再生器温度[500-800] 实际[600-730]
                            _regenerator_temp_increment = _regenerator_temp_value < 500 ? 5 : 2;
                            _regenerator_temp_increment = _regenerator_temp_value > 800 ? 0 : 2;
                            _regenerator_temp_value += _regenerator_temp_increment;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);

                        }
                        else
                        {
                            if (!Isinner)
                                ValveInCatalyst_item.Close();
                            FlowInCatalyst_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }

                });

                #endregion
                #endregion

                #region 油料进入提升管反应器
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveInMixedOil_state)
                        {
                            if (!Isinner) ValveInMixedOil_item.Open();
                            FlowInMixedOil_item.StartFlow();
                            //油料进入,提升管温度上升[350-550] 实际[400-500]
                            _riserReactor_temp_increment = _riserReactor_temp_value < 350 ? 5 : 3;
                            _riserReactor_temp_increment = _riserReactor_temp_value > 550 ? 0 : 3;
                            _riserReactor_temp_value += _riserReactor_temp_increment;
                            LabelTempRiserReactor_item.Write(_riserReactor_temp_value);
                                                       
                        }
                        else
                        {
                            if (!Isinner)
                                ValveInMixedOil_item.Close();
                            FlowInMixedOil_item.StopFlow();

                        }
                        Thread.Sleep(_sleepfast);

                    }
                });
                #endregion
            
                #region  蒸汽雾化,与高温催化剂反应
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {

                        if(ValveInSteam_state)
                        {
                            if (!Isinner)
                                ValveInSteam_item.Open();
                            FlowInSteam_item.StartFlow();
                            //提升管反应器温度继续上升,温度[350,550]  实际[400,500]
                            _riserReactor_temp_increment = + _riserReactor_temp_value < 400 ? 5 :3;
                            _riserReactor_temp_increment = _riserReactor_temp_value > 500 ? 0 : 3;
                            _riserReactor_temp_value += _riserReactor_temp_increment;
                            LabelTempRiserReactor_item.Write(_riserReactor_temp_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveInSteam_item.Close();
                            FlowInSteam_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                 
                });

                #endregion

                #region 来自再生器的催化剂
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveRiserReactorCatalyst_state)
                        {
                            if (!Isinner)
                                ValveRiserReactorCatalyst_item.Open();
                            FlowRiserReactorCatalyst_item.StartFlow();
                            //催化剂从再生器流出，温度下降,范围[450-500] 实际[500-550]
                            _regenerator_temp_decrement = _regenerator_temp_value < 550 ? 1 : 8;
                            _regenerator_temp_decrement = _regenerator_temp_value > 500 ? 1: 0;
                            _regenerator_temp_value -= _regenerator_temp_decrement;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);
                            //烧焦罐压力下降
                            _charringTank_pressure_decrement = _charringTank_pressure_value < 0.3 ? 0 : 0.01;
                            _charringTank_pressure_decrement = _charringTank_pressure_value > 0.7 ? 0.03 : 0.01;
                            _charringTank_pressure_value -= _charringTank_pressure_decrement;
                            LabelPressureCharringTank_item.Write(_charringTank_pressure_value);


                            //催化剂进入，反应开始，提升管反应器温度继续上升
                            _riserReactor_temp_increment = +_riserReactor_temp_value < 400 ? 5 : 3;
                            _riserReactor_temp_increment = _riserReactor_temp_value > 500 ? 0 : 3;
                            _riserReactor_temp_value += _riserReactor_temp_increment;
                            LabelTempRiserReactor_item.Write(_riserReactor_temp_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveRiserReactorCatalyst_item.Close();
                            FlowRiserReactorCatalyst_item.StopFlow();

                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 进入沉降室，汽提蒸汽进入
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                      if(ValveInStrippedVapor_state)
                        {
                            if (!Isinner)
                                ValveInStrippedVapor_item.Open();
                            FlowStrippedVapor_item.StartFlow();
                            //容量一定，气压与温度成正比
                            //500℃左右的油气,沉降室压力范围[0.1-0.3]  标准[0.13-0.2]
                            _settlingChamber_pressure_increment = _settlingChamber_pressure_value < 0.1 ? 0.05 : 0.01;
                            _settlingChamber_pressure_increment = _settlingChamber_pressure_value > 0.3 ? 0 : 0.01;
                            _settlingChamber_pressure_value += _settlingChamber_pressure_increment;
                            LabelPressureSettlingChamber_item.Write(_settlingChamber_pressure_value);
                            //提升管温度下降
                 

                        }
                      else
                        {
                            if (!Isinner)
                                ValveInStrippedVapor_item.Close();
                            FlowStrippedVapor_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #endregion

                #region  沉降室气压（气压机调节气压？）
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)//引风机降压
                    {
                        if (ValveDraftFan_state)
                        {
                            if (!Isinner)
                                ValveDraftFan_item.Open();
                            FlowOutAir_item.StartFlow();
                            _settlingChamber_pressure_decrement = _settlingChamber_pressure_value > 0.3 ? 0.02 : 0.01;
                            _settlingChamber_pressure_decrement = _settlingChamber_pressure_value < 0.1 ? 0 : 0.01;
                            _settlingChamber_pressure_value -= _settlingChamber_pressure_decrement;
                            LabelPressureSettlingChamber_item.Write(_settlingChamber_pressure_value);

                        }
                        else
                        {
                            if (!Isinner)
                                ValveDraftFan_item.Close();
                            FlowOutAir_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion


                #endregion

                #region 再生系统

                #region 经汽提后的油焦催化剂混合物进入烧焦罐底
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveStrippedCatalyst_state)
                        {
                            if (!Isinner)
                                ValveStrippedCatalyst_item.Open();
                            FlowStrippedCatalyst_item.StartFlow();
                            FlowInFuelOil_item.StartFlow();
                            //烧焦罐压力[0.3-0.7]  实际[0.4-0.6]
                            _charringTank_pressure_increment = _charringTank_pressure_value < 0.3 ? 0.03 : 0.01;
                            _charringTank_pressure_increment = _charringTank_pressure_value > 0.7 ? 0 : 0.01;
                            _charringTank_pressure_value += _charringTank_pressure_increment;
                            LabelPressureCharringTank_item.Write(_charringTank_pressure_value);

                        }
                        else
                        {
                            if (!Isinner)

                               ValveStrippedCatalyst_item.Close();
                            ValveStrippedCatalyst_item.StopFlow();
                            FlowInFuelOil_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #region 外取热器(油焦催化剂混合物)流向烧焦罐底
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if (ValveRegCatalyst_state)
                        {
                                if (!Isinner)
                                ValveRegCatalyst_item.Open();
                            FlowRegCatalyst_item.StartFlow();
                            _charringTank_pressure_increment = _charringTank_pressure_value < 0.3 ? 0.02 : 0.01;
                            _charringTank_pressure_increment = _charringTank_pressure_value > 0.7 ? 0 : 0.01;
                            _charringTank_pressure_value += _charringTank_pressure_increment;
                            LabelPressureCharringTank_item.Write(_charringTank_pressure_value);
                        }
                        else
                        {
                            if (!Isinner)
                                ValveRegCatalyst_item.Close();
                            FlowRegCatalyst_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);



                    }
                });
                #endregion
                #region 调节主风机风量，助燃,油焦分解
                Task.Run(()=> 
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInAir_state && ValveStrippedCatalyst_state)
                        {
                            if (!Isinner)
                            {
                                ValveInAir_item.Open();
                                ValveStrippedCatalyst_item.Open();
                            }
                            FlowInAir_item.StartFlow();
                            FlowStrippedCatalyst_item.StopFlow();

                            //主风机流量[1000-1400] 实际[1280-1350]
                            _flow_air_increment = _flow_air_value < 1000 ? 30 : 10;
                            _flow_air_increment = _flow_air_value > 1400 ? 0: 10;
                            _flow_air_value += _flow_air_increment;
                            LabelFlowAir_item.Write(_flow_air_value);
                            //再生器温度升高，进行油焦分解
                            _regenerator_temp_increment = _regenerator_temp_value < 600 ? 5 : 3;
                            _regenerator_temp_increment = _regenerator_temp_value > 730 ? 0 : 3;
                            _regenerator_temp_value += _regenerator_temp_increment;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);

                        }
                        else
                        {
                            if (!Isinner)
                            {
                                ValveInAir_item.Close();
                                ValveStrippedCatalyst_item.Close();

                            }
                            FlowStrippedCatalyst_item.StopFlow();
                            FlowInAir_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);

                    }

                });
                
                #endregion



                #endregion

                #region 再生催化剂三路
                //第一路经再生斜管去提升管反应器,(油焦完成分解)
                Task.Run(()=>
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveRiserReactorCatalyst_state)
                        {
                            if (!Isinner)
                                ValveRiserReactorCatalyst_item.Open();
                            FlowRiserReactorCatalyst_item.StartFlow();
                        }
                        else
                        {
                            if (!Isinner)
                                ValveRiserReactorCatalyst_item.Close();
                            FlowRiserReactorCatalyst_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }

                });
                //外取热器保持再生器内热平衡
                //第二路经外取热上斜管去外取热器，
                Task.Run(()=>
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveExternalCoolerCatalyst_state)
                        {
                            if (!Isinner)
                                ValveExternalCoolerCatalyst_item.Open();
                            FlowExternalCoolerCatalyst_item.StartFlow();
                            //过热器温度过高，保持热平衡，外取热器液位相对下降
                            if(_regenerator_temp_value>730)
                            {
                                _exCooler_level_decrement = _regenerator_temp_value > 730 ? 1 : 0;
                                _exCooler_level_value -= _exCooler_level_decrement;
                                LabelLevel_item.Write(_exCooler_level_value);

                            }
                         
                            //再生器温度下降
                            _regenerator_temp_decrement = _regenerator_temp_value < 600 ? 1 : 8;
                            _regenerator_temp_decrement = _regenerator_temp_value > 730 ? 0 : 8;
                            _regenerator_temp_value -= _regenerator_temp_decrement;
                            LabelTempRegenerator_item.Write(_regenerator_temp_value);
          

                        }
                        else
                        {
                            if (!Isinner)
                                ValveExternalCoolerCatalyst_item.Close();
                            FlowExternalCoolerCatalyst_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                //第三路经循环斜管去烧焦罐（油焦未完成分解）
                Task.Run(()=> 
                {
                    while(!_listenCts.IsCancellationRequested)
                    {
                        if(ValveCharringTankCatalyst_state)
                        {
                            if (!Isinner)
                                ValveCharringTankCatalyst_item.Open();
                            FlowCharringTankCatalyst_item.StartFlow();
                        }
                        else
                        {
                            if (!Isinner)
                                ValveCharringTankCatalyst_item.Close();
                            FlowCharringTankCatalyst_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion 
          
                #endregion
            });

        }
        private void CalcFlowAndStep()
        {
            #region 流程步骤
            //1预热准备
            //1.1 外取热器上水
   
            //var cold_open_item = _steps.Where(i => i.Status[StatusName.冷却水阀门] == open).OrderByDescending(i => i.Date).FirstOrDefault();
            //var cold_close_item = cold_open_item != null ? _steps.Where(i => i.Status[StatusName.冷却水阀门] == close && i.Date > cold_open_item.Date).OrderBy(i => i.Date).FirstOrDefault() : null;
            //var level_cold_item = cold_close_item != null ? _steps.Where(i => i.Status[StatusName.冷却水阀门] == close && i.Date > cold_close_item.Date)
            //    .OrderByDescending(i => i.Status[StatusName.外取热器液位]).ThenByDescending(i => i.Date).FirstOrDefault() : null;
            var level_cold_item = _steps.Where(i => i.Status[StatusName.冷却水阀门] == open).OrderByDescending(i => i.Status[StatusName.外取热器液位]).ThenByDescending(i => i.Date).FirstOrDefault();
            //1.2 烧焦罐压力
            var fueloil_open_item = _steps.Where(i => i.Status[StatusName.燃料油阀门] == open).OrderBy(i => i.Date).FirstOrDefault();
            //var air_openfirst_item = fueloil_open_item != null ? _steps.Where(i => i.Status[StatusName.空气阀门] == open && i.Date > fueloil_open_item.Date).OrderBy(i => i.Date).FirstOrDefault() : null;
            var avg_presscharringtank_count = _steps.Count(i => i.Status[StatusName.燃料油阀门] == open && i.Status[StatusName.燃料油阀门] == open);
            var avg_presscharringtank = avg_presscharringtank_count > 0 ? _steps.Where(i => i.Status[StatusName.燃料油阀门] == open && i.Status[StatusName.空气阀门] == open)
                .Average(i => i.Status[StatusName.烧焦罐压力]) : -1;
            //1.3空气流量
            var air_open_item = _steps.Where(i => i.Status[StatusName.空气阀门] == open)
               .OrderBy(i => i.Date).FirstOrDefault();
            var avg_flowair_count = _steps.Count(i => i.Status[StatusName.空气阀门] == open);
            var avg_flowair = avg_flowair_count > 0 ? _steps.Where(i => i.Status[StatusName.空气阀门] == open)
                .Average(i => i.Status[StatusName.主风机流量]) : -1;

            //1.4 再生器温度
            var avg_tempregenerator_count = _steps.Count(i => i.Status[StatusName.燃料油阀门] == open
             && i.Status[StatusName.空气阀门] == open);
            var avg_tempregenerator = avg_tempregenerator_count > 0 ? _steps.Where(i => i.Status[StatusName.燃料油阀门] == open
                  && i.Status[StatusName.空气阀门] == open).Average(i => i.Status[StatusName.再生器温度]) : -1;
            //1.5 催化剂温度（>700）
            //var avg_tempregenerator_count = _steps.Count(i => i.Status[StatusName.催化剂阀门] == open && i.Status[StatusName.燃料油阀门] == open
            //&& i.Status[StatusName.空气阀门] == open);
            //var avg_tempregenerator = avg_tempregenerator_count > 0 ? _steps.Where(i => i.Status[StatusName.催化剂阀门] == open && i.Status[StatusName.燃料油阀门] == open
            //      && i.Status[StatusName.空气阀门] == open).Average(i => i.Status[StatusName.再生器温度]) : -1;
            var catalysis_temper_item=_steps.Where(i=>i.Status[StatusName.催化剂阀门]==open).OrderByDescending(i => i.Status[StatusName.再生器温度]).ThenByDescending(i => i.Date).FirstOrDefault();
            //1.6 再生器压力(**)
            //var avg_pressregenerator = avg_tempregenerator_count > 0 ? _steps.Where(i => i.Status[StatusName.催化剂阀门] == open && i.Status[StatusName.燃料油阀门] == open
            //        && i.Status[StatusName.空气阀门] == open).Average(i => i.Status[StatusName.再生器压力]) : -1;

            //2 反应系统
            //2.1 提升管反应器温度
            var mixedoil_open_item = _steps.Where(i => i.Status[StatusName.混合油阀门] == open).OrderBy(i => i.Date).FirstOrDefault();
            var mixedoil_close_item = _steps.Where(i => i.Status[StatusName.混合油阀门] == close).OrderBy(i => i.Date).FirstOrDefault();
            var avg_risereactor_count = _steps.Count(i => i.Status[StatusName.混合油阀门] == open && i.Status[StatusName.水蒸气阀门] == open &&
              i.Status[StatusName.再生催化剂流入提升反应器阀门] == open);
            var avg_risereactor = avg_risereactor_count > 0 ? _steps.Where(i => i.Status[StatusName.混合油阀门] == open && i.Status[StatusName.水蒸气阀门] == open &&
                    i.Status[StatusName.再生催化剂流入提升反应器阀门] == open).Average(i => i.Status[StatusName.提升管反应器温度]) : -1;
            //2.2 沉降室压力(汽提蒸汽阀，引风机，油焦下放阀同时打开，控制其压力)
            var avg_presetchamber_count = _steps.Count(i=>i.Status[StatusName.汽提蒸汽阀门]==open);
            var avg_presetchamber = avg_presetchamber_count > 0 ? _steps.Where(i => i.Status[StatusName.汽提蒸汽阀门] == open ).Average(i => i.Status[StatusName.沉降室压力]) : -1;
            ////2.3沉降室压力(引风机开，控制其压力)
            //var avg_presetchamber1_count =_steps.Count(i => i.Status[StatusName.引风机阀门] == open&& i.Status[StatusName.汽提蒸汽阀门] == open);
            //var avg_presetchamber1 = avg_presetchamber1_count > 0 ? _steps.Where(i => i.Status[StatusName.引风机阀门] == open && i.Status[StatusName.汽提蒸汽阀门] == open).Average(i => i.Status[StatusName.沉降室压力]) : -1;

            //3 再生系统
            //3.1主风流量（油焦反应时）         
            var avg_flowair1_count = _steps.Count(i => i.Status[StatusName.空气阀门] == open&&i.Status[StatusName.再生催化剂流入烧焦罐阀门]==open);
            var avg_flwair1= avg_flowair1_count>0? _steps.Where(i => i.Status[StatusName.空气阀门] == open && i.Status[StatusName.再生催化剂流入烧焦罐阀门] == open)
                .Average(i => i.Status[StatusName.主风机流量]) : -1;

            //3.2烧焦罐压力(油焦反应时)
            var avg_presschartank1_count = _steps.Count(i => i.Status[StatusName.再生催化剂流入烧焦罐阀门] == open && i.Status[StatusName.再生催化剂从外取热器流入烧焦罐阀门] == open
              && i.Status[StatusName.燃料油阀门] == open && i.Status[StatusName.空气阀门] == open);
            var avg_presschartank1 = avg_presschartank1_count > 0 ? _steps.Where(i => i.Status[StatusName.再生催化剂流入烧焦罐阀门] == open && i.Status[StatusName.再生催化剂从外取热器流入烧焦罐阀门] == open
                    && i.Status[StatusName.燃料油阀门] == open && i.Status[StatusName.空气阀门] == open).Average(i => i.Status[StatusName.烧焦罐压力]) : -1;
            //3.3再生器温度（反应时）
            var avg_tempregenerator1 = avg_presschartank1_count > 0 ? _steps.Where(i => i.Status[StatusName.再生催化剂流入烧焦罐阀门] == open && i.Status[StatusName.再生催化剂从外取热器流入烧焦罐阀门] == open             
            && i.Status[StatusName.燃料油阀门] == open && i.Status[StatusName.空气阀门] == open).Average(i => i.Status[StatusName.再生器温度]) : -1;
            //4阀门顺序
            //4.1 燃料油阀门打开
            //var fueloil_open_item = _steps.Where(i => i.Status[StatusName.燃料油阀门] == open).OrderByDescending(i => i.Date).FirstOrDefault();
            //4.2  空气阀门打开
            //var air_open_item 
            //=_steps.Where(i => i.Status[StatusName.空气阀门] == open )
            //    .OrderBy(i => i.Date).FirstOrDefault();
            //4.3 催化剂进料阀门打开
            var catalysis_open_item = _steps.Where(i => i.Status[StatusName.催化剂阀门] == open)
                .OrderBy(i => i.Date).FirstOrDefault();


            #endregion

            #region 计算分数
        if (level_cold_item!=null)
            {
                var level_cold = level_cold_item.Status[StatusName.外取热器液位];
                if(level_cold > 240&& level_cold<360)
                {
                    AddCompose("冷却水上水：冷却水在正常范围40%-60%");
                }
                else if (level_cold < 240)
                {
                    AddCompose("冷却水上水：冷水低于正常值的40%");
                }
                else
                {
                    AddCompose("冷却水上水：冷却水高于正常值的60%");
                }
            }

            if(avg_presscharringtank!=-1)
            {
                if(avg_presscharringtank>0.4&&avg_presscharringtank<0.6)
                {
                    AddCompose("开炉：烧焦罐压力在正常范围：0.4-0.36MPa");
                }
                else if(avg_presscharringtank<0.4)
                {
                    AddCompose("开炉：烧焦罐压力低于0.4Mpa");
                }
                else
                {
                    AddCompose("开炉：烧焦罐压力大于0.6Mpa");
                }

            }
            if(avg_flowair!=-1)
            {
                if(avg_flowair>380&&avg_flowair<550)
                {
                    AddCompose("主风机打开：主风量在正常范围380-550m3/h");
                }
                else if(avg_flowair<380)
                {
                    AddCompose("主风机打开：主风量在小于380m3/h");
                }
                else
                {
                    AddCompose("主风机打开：主风量大于500m3/h");
                }

            }
            if(avg_tempregenerator!=-1)
            {
                if(avg_tempregenerator>500 && avg_tempregenerator<550)
                {
                    AddCompose("催化剂原料上料：再生器温度在正常范围500-550℃");
                }
                else if(avg_tempregenerator<500)
                {
                    AddCompose("催化剂原料上料：再生器温度低于500℃");
                }
                else
                {
                    AddCompose("催化剂原料上料：再生器温度高于550℃");
                }
            }
           // catalysis_temper
           if(catalysis_temper_item!=null)
            {
                var catalysis_temper = catalysis_temper_item.Status[StatusName.再生器温度];
                if(catalysis_temper>700)
                {
                    AddCompose("催化剂参与反应：温度正常在700℃以上");
                }
                else
                {
                    AddCompose("催化剂参与反应:温度小于700℃");
                }
            }
               
            //if (avg_pressregenerator!=-1)
            //{
                
            //}
            if(avg_risereactor!=-1)
            {
                if(avg_risereactor>400 && avg_risereactor<500)
                {
                    AddCompose("反应进行：提升管温度在正常范围400-500℃");

                }
                else if(avg_risereactor<400)
                {
                    AddCompose("反应进行：提升管温度低于400℃");
                }
                else
                {
                    AddCompose("反应进行：提升管温度高于500℃ ");
                }
            }
            if(avg_presetchamber!=-1)
            {
                if(avg_presetchamber>0.13 && avg_presetchamber<0.2)
                {
                    AddCompose("反应进行：沉降室压力在正常范围0.1-0.2MPa");
                }
                else if(avg_presetchamber>0.2)
                {
                    AddCompose("反应进行：沉降室压力高于0.2MPa");
                }
                else
                {
                    AddCompose("反应进行：沉降室压力小于0.13MPa");

                }
            }
            //if(avg_presetchamber1!=-1)
            //{
            //    if(avg_presetchamber1 > 0.13 && avg_presetchamber < 0.2)
            //    {
            //        AddCompose("引风机打开：调节沉降室压力在正常范围0.1-0.2MPa");
            //    }
            //    else if (avg_presetchamber > 0.2)
            //    {
            //        AddCompose("引风机打开：调节沉降室压力高于0.2MPa");
            //    }
            //    else
            //    {
            //        AddCompose("引风机打开：调节沉降室压力小于0.13MPa");

            //    }

            //}
            if(avg_flwair1!=-1)
            {
                if(avg_flwair1>1000&&avg_flwair1<1200)
                {
                    AddCompose("再生过程：主风量在正常范围1000-1200m3/h");
                }
                else if(avg_flwair1<1280)
                {
                    AddCompose("再生过程：主风量小于1000m3/h");
                }
                else
                {
                    AddCompose("再生过程：主风量大于1200m3/h");
                }
                
            }
            if(avg_presschartank1!=1)
            {
                if(avg_presschartank1>0.4 && avg_presschartank1<0.6)
                {
                    AddCompose("再生过程：烧焦罐压力在正常范围0.4-0.6MPa");
                }
                else if(avg_presschartank1<0.4)
                {
                    AddCompose("再生过程：烧焦罐压力小于0.4MPa");

                }
                else
                {
                    AddCompose("再生过程：烧焦罐压力大于0.6MPa");
                }
            }
            //
            if (avg_tempregenerator1 != -1)
            {
                if (avg_tempregenerator1 > 600 && avg_tempregenerator1 < 730)
                {
                    AddCompose("再生过程：再生器温度在正常范围600-730℃");
                }
                else if (avg_tempregenerator1 < 600)
                {
                    AddCompose("再生过程：再生器温度低于600℃");
                }
                else
                {
                    AddCompose("再生过程：再生器温度高于730℃");
                }
            }
            if(air_open_item!=null&&fueloil_open_item!=null)
            {
                if(air_open_item.Date>fueloil_open_item.Date)
                {
                    AddCompose("操作正确：空气阀门在燃料油阀门后打开");
                }
                else
                {
                    AddCompose("操作错误：空气阀门早于燃料油阀门打开");
                }
            }
            if(air_open_item!=null && catalysis_open_item!=null)
            {
                if(air_open_item.Date<catalysis_open_item.Date)
                {
                    AddCompose("操作正确：空气阀门早于催化剂原料阀门打开");
                }
                else
                {
                    AddCompose("操作错误：催化剂原料阀门早于空气阀门打开");
                }
            }
            #endregion


        }
        #endregion
        #region 枚举
        enum FiledName
        {
            //阀门
            ValveInStrippedVapor,
            ValveInCatalyst,
            ValveInMixedOil,
            ValveInSteam,
            ValveStrippedCatalyst,
            ValveInAir,
            ValveRiserReactorCatalyst,
            ValveCharringTankCatalyst,
            ValveExternalCoolerCatalyst,
            ValveRegCatalyst,
            ValveInFuelOil,
            ValveInCold,
            ValveDraftFan,
            //管道
            FlowStrippedVapor,
            FlowInCatalyst,
            FlowInMixedOil,
            FlowStrippedCatalyst,
            FlowInAir,
            FlowRiserReactorCatalyst,
            FlowCharringTankCatalyst,
            FlowExternalCoolerCatalyst,
            FlowRegCatalyst,
            FlowInSteam,
            FlowInFuelOil,
            FlowInCold,
            FlowOutAir,
            //标签
            LabelPressureSettlingChamber,
            LabelPressureCharringTank,
            LabelTempRiserReactor,
            LabelFlowAir,
            LabelTempRegenerator,
            LabelPressureRegenerator,
            LabelLevel,
            //闪烁
            FlashPressureSettlingChamber,
            FlashlPressureCharringTank,
            FlashTempRiserReactor,
            FlashFlowAir,
            FlashTempRegenerator,
            FlashPressureRegenerator,
            FlashPressurExCooler,


        }
        enum StatusName
        {
            汽提蒸汽阀门,
            催化剂阀门,
            混合油阀门,
            水蒸气阀门,
            汽提催化剂阀门,
            空气阀门,
            再生催化剂流入提升反应器阀门,
            再生催化剂流入烧焦罐阀门,
            再生催化剂流入外取热器阀门,
            再生催化剂从外取热器流入烧焦罐阀门,
            沉降室压力,
            烧焦罐压力,
            提升管反应器温度,
            主风机流量,
            再生器温度,
            再生器压力,
            外取热器液位,
            燃料油阀门,
            冷却水阀门,
            引风机阀门,
            


        }
      
        #endregion

    }
}

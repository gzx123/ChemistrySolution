﻿using Chemistry.Models;
using OPCAutomation;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Chemistry.WorkFlow
{
    /// <summary>
    /// 聚丙烯生产聚合自控流程
    /// </summary>
    public class Polypropylene_Step : FlowBaseStep
    {
        #region 字段
        private int _sleepfast = 500;
        private static int _relay_command = 0;

        static double _pressure_value = 0;
        static double _temperature_value = 22;
        static double _flow_value = 100;
        static int _reaction_second = 40;//反应时间（分钟）
        static double _h2_flow;

        //初始值 用来被减去的值
        static double _total_pressure_h2 ;
        static double _total_pressure_hopper;
        static double _total_weight1 ;
        static double _total_weight2;
        static double _pressure_h2 ;
        
        static double _pressure_hopper ;
        static double _weight_left1 ;
        static double _weight_left2 ;
        static double _weight_right ;

        int open = 1;//0-48
        int close = 0;//1-49
        

        static bool _isgetoverstate1 = false;
        static bool _isgetoverstate2 = false;
        static bool _isgetoverstate3 = false;

        static double _temperature_increment ;
        static double _temperature_decrement ;
        static double _pressure_increment ;
        static double _pressure_decrement;
        static double _pressure_hopper_crement;
        static double _h2_flow_crement;
        //命令
        private Tools.Tool.FlowCommand _flowcommand;
        string _switch1 = string.Empty;
        string _switch2 = string.Empty;
        string _prefix1 = string.Empty;
        string _prefix2 = string.Empty;
        string _prefix3 = string.Empty;
        string _llcmd = string.Empty;
        string _wdcmd = string.Empty;
        string _ywcmd = string.Empty;
        string _ylcmd = string.Empty;
        string _heightcmd = string.Empty;
        string _aicmd = string.Empty;
        string _relaycmd = string.Empty;
        int _warning1number ;
        int _warning2number;
        int _fognumber ;
        int _stirnumber;

        #endregion

        #region 与组态对应的阀门状态
        //开关状态
        static bool ValveLeftEmpty_State = false;
        static bool ValveOutO2_State = false;
        static bool ValveInActivator_State = false;
        static bool ValveInCold_State = false;
        static bool ValveInColdSecond_State = false;
        static bool ValveInGas_State = false;
        static bool ValveInH2_State = false;
        static bool ValveInHot_State = false;
        static bool ValveInN2Right_State = false;
        static bool ValveInPropyleneLeft_State = false;
        static bool ValveInPropyleneRight_State = false;
        static bool ValveLeftHopper_State = false;
        //static bool ValveOutCold_State = false;
        static bool ValveOutGas_State = false;
        //static bool ValveOutHot_State = false;
        static bool ValveOutPropylene_State = false;
        static bool ValveOutPropyleneWithHopper_State = false;
        static bool ValveRightHopper_State = false;
        //static bool ValveEmptyFromHopper_State = false;
        //static bool ValveInDownPropylene_State = false;
        static bool ValveStir_State = false;
        static bool ValveStop_State = false;

        #endregion

        #region OPCItem
        //阀门
        OPCItem ValveLeftEmpty_item;
        /// <summary>
        /// 放空总管
        /// </summary>
        OPCItem ValveOutO2_item;
        OPCItem ValveInActivator_item;
        OPCItem ValveInCold_item;
        OPCItem ValveInColdSecond_item;
        /// <summary>
        /// 气相丙烯
        /// </summary>
        OPCItem ValveInGas_item;
        OPCItem ValveInH2_item;
        OPCItem ValveInHot_item;
        OPCItem ValveInN2Right_item;
        OPCItem ValveInPropyleneLeft_item;
        OPCItem ValveInPropyleneRight_item;
        OPCItem ValveLeftHopper_item;
        OPCItem ValveOutCold_item;
        OPCItem ValveOutGas_item;
        OPCItem ValveOutHot_item;
        OPCItem ValveOutPropylene_item;
        OPCItem ValveOutPropyleneWithHopper_item;
        OPCItem ValveRightHopper_item;
        OPCItem ValveInDownPropylene_item;
        OPCItem ValveEmptyFromHopper_item;
        OPCItem ValveStir_item;
        OPCItem ValveStop_item;
        //管道
        OPCItem FlowOutO2_item;
        OPCItem FlowInPropylene_item;
        OPCItem FlowInActivator_item;
        OPCItem FlowInN2_item;
        OPCItem FlowInCold_item;
        OPCItem FlowInColdSecond_item;
        OPCItem FlowInGas_item;
        OPCItem FlowInH2_item;
        OPCItem FlowInHot_item;
        OPCItem FlowInLeft_item;
        OPCItem FlowInPropyleneRight_item;
        OPCItem FlowInRight_item;
        OPCItem FlowOutActivator_item;
        OPCItem FlowLeftEmpty_item;
        OPCItem FlowOutPropylene_item;
        OPCItem FlowOutCold_item;
        OPCItem FlowOutGas_item;
        OPCItem FlowOutHot_item;
        OPCItem FlowOutGasFomKettle_item;
        OPCItem FlowInPropyleneDown_item;
        OPCItem FlowRightEmpty_item;

        //label
        OPCItem LabelPressure_item;
        OPCItem LabelTemperature_item;
        OPCItem LabelFlow_item;
        OPCItem LabelContent_item;
        OPCItem LabelH2press_item;
        OPCItem LabelWeight1_item;
        OPCItem LabelWeight2_item;
        OPCItem LabelPressureHopper_item;//活化剂压力

        //闪烁
        OPCItem FlashKettle_item;
        OPCItem FlashPressure_item;
        OPCItem FlashTemperature_item;
        OPCItem FlashHopperPressure_item;
        OPCItem FlashWarning_item;
        #endregion

        #region 初始化

        public Polypropylene_Step(OPCServer opcserver, SerialPort port, bool isinner, List<ExaminationPoint> points)
        {
            _opcServer = opcserver;
            _serialPort = port;
            Isinner = isinner;
            base._points = points;
            InitFiled();
            InitOPC();
            InitOPCItem();
            InitSignal();
        }

     

        private void InitFlowCommandData()
        {
            string classname = this.GetType().Name;
            classname = classname.Substring(0, classname.Length - 5);
            _flowcommand = Tools.Tool.GetFlowCommand(classname);
            _switch1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch1").Value;
            _switch2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "switch2").Value;
            _prefix1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix1").Value;
            _prefix2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix2").Value;
            _prefix3 = _flowcommand.Items.SingleOrDefault(i => i.Name == "prefix3").Value;
            _llcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "llcmd").Value;
            _ywcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ywcmd").Value;
            _ylcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "ylcmd").Value;
            _heightcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "heightcmd").Value;
            _wdcmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "wdcmd").Value;
            _aicmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "aicmd").Value;
            _relaycmd = _flowcommand.Items.SingleOrDefault(i => i.Name == "relaycmd").Value;
            _warning1number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning1").Value);
            _warning2number = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "warning2").Value);
            _fognumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "fog").Value);
            _stirnumber = int.Parse(_flowcommand.Items.SingleOrDefault(i => i.Name == "stir").Value);
        }

        private void InitSignal()
        {
            if (Isinner)
            {
                ListenSignal();
                open = 1;
                close = 0;
            }
            else
            {
                InitFlowCommandData();
                _serialPort.DataReceived += _serialPort_DataReceived;
                InitOPCItemWithSerialPort();
                ListenSerialPort();
                open = 48;
                close = 49;
            }
        }

        private void InitFiled()
        {
            _pressure_value = 0;
            _temperature_value = 22;
            _flow_value = 100;
            _h2_flow = 0;

            _total_pressure_h2 = 0.3;
            _total_pressure_hopper = 0.3;
            _total_weight1 = 6000;
            _total_weight2 = 6000;
            _pressure_h2 = 0;
            _pressure_hopper = 0;
            _weight_left1 = 0;
            _weight_left2 = 0;
            _weight_right = 0;

            _isgetoverstate1 = false;
            _isgetoverstate2 = false;
            _isgetoverstate3 = false;

            _temperature_increment = 1;
            _temperature_decrement = 1;
            _pressure_increment = 0.01;
            _pressure_decrement = 0.04;
            _pressure_hopper_crement = 0.01;
            _h2_flow_crement = 2;
        }

        private void InitOPCItemWithSerialPort()
        {
            SpHeightValue(1, _total_weight1);//左侧丙烯重量
            SpPressureValue(_pressure_value);//压力
            SpHeightValue(4, _total_weight2);//右侧丙烯重量
            SpTemperatureValue(_temperature_value);//温度
            SpFlowValue(_h2_flow);
        }

        
        private void InitOPCItem()
        {
            //阀门
            ValveLeftEmpty_item = GetItem(FieldName.ValveLeftEmpty); ValveLeftEmpty_item.Close();
            ValveOutO2_item = GetItem(FieldName.ValveOutO2); ValveOutO2_item.Close();
            ValveInActivator_item = GetItem(FieldName.ValveInActivator); ValveInActivator_item.Close();
            ValveInCold_item = GetItem(FieldName.ValveInCold); ValveInCold_item.Close();
            ValveInColdSecond_item = GetItem(FieldName.ValveInColdSecond); ValveInColdSecond_item.Close();
            ValveInGas_item = GetItem(FieldName.ValveInGas); ValveInGas_item.Close();
            ValveInH2_item = GetItem(FieldName.ValveInH2); ValveInH2_item.Close();
            ValveInHot_item = GetItem(FieldName.ValveInHot); ValveInHot_item.Close();
            ValveInN2Right_item = GetItem(FieldName.ValveInN2Right); ValveInN2Right_item.Close();
            ValveInPropyleneLeft_item = GetItem(FieldName.ValveInPropyleneLeft); ValveInPropyleneLeft_item.Close();
            ValveInPropyleneRight_item = GetItem(FieldName.ValveInPropyleneRight); ValveInPropyleneRight_item.Close();
            ValveLeftHopper_item = GetItem(FieldName.ValveLeftHopper); ValveLeftHopper_item.Close();
            ValveOutCold_item = GetItem(FieldName.ValveOutCold); ValveOutCold_item.Close();
            ValveOutGas_item = GetItem(FieldName.ValveOutGas); ValveOutGas_item.Close();
            ValveOutHot_item = GetItem(FieldName.ValveOutHot); ValveOutHot_item.Close();
            ValveOutPropylene_item = GetItem(FieldName.ValveOutPropylene); ValveOutPropylene_item.Close();
            ValveOutPropyleneWithHopper_item = GetItem(FieldName.ValveOutPropyleneWithHopper); ValveOutPropyleneWithHopper_item.Close();
            ValveRightHopper_item = GetItem(FieldName.ValveRightHopper); ValveRightHopper_item.Close();
            ValveInDownPropylene_item = GetItem(FieldName.ValveInDownPropylene); ValveInDownPropylene_item.Close();
            ValveEmptyFromHopper_item = GetItem(FieldName.ValveEmptyFromHopper); ValveEmptyFromHopper_item.Close();
            ValveStir_item = GetItem(FieldName.ValveStir); ValveStir_item.Close();
            ValveStop_item = GetItem(FieldName.ValveStop); ValveStop_item.Close();

            //管道
            FlowOutO2_item = GetItem(FieldName.FlowOutO2); FlowOutO2_item.Close();
            FlowInPropylene_item = GetItem(FieldName.FlowInPropylene); FlowInPropylene_item.Close();
            FlowInActivator_item = GetItem(FieldName.FlowInActivator); FlowInActivator_item.Close();
            FlowInN2_item = GetItem(FieldName.FlowInN2); FlowInN2_item.Close();
            FlowInCold_item = GetItem(FieldName.FlowInCold); FlowInCold_item.Close();
            FlowInColdSecond_item = GetItem(FieldName.FlowInColdSecond); FlowInColdSecond_item.Close();
            FlowInGas_item = GetItem(FieldName.FlowInGas); FlowInGas_item.Close();
            FlowInH2_item = GetItem(FieldName.FlowInH2); FlowInH2_item.Close();
            FlowInHot_item = GetItem(FieldName.FlowInHot); FlowInHot_item.Close();
            FlowInLeft_item = GetItem(FieldName.FlowInLeft); FlowInLeft_item.Close();
            FlowInPropyleneRight_item = GetItem(FieldName.FlowInPropyleneRight); FlowInPropyleneRight_item.Close();
            FlowInRight_item = GetItem(FieldName.FlowInRight); FlowInRight_item.Close();
            FlowOutActivator_item = GetItem(FieldName.FlowOutActivator); FlowOutActivator_item.Close();
            FlowLeftEmpty_item = GetItem(FieldName.FlowLeftEmpty); FlowLeftEmpty_item.Close();
            FlowOutPropylene_item = GetItem(FieldName.FlowOutPropylene); FlowOutPropylene_item.Close();
            FlowOutCold_item = GetItem(FieldName.FlowOutCold); FlowOutCold_item.Close();
            FlowOutGas_item = GetItem(FieldName.FlowOutGas); FlowOutGas_item.Close();
            FlowOutHot_item = GetItem(FieldName.FlowOutHot); FlowOutHot_item.Close();
            FlowOutGasFomKettle_item = GetItem(FieldName.FlowOutGasFomKettle); FlowOutGasFomKettle_item.Close();
            FlowInPropyleneDown_item = GetItem(FieldName.FlowInPropyleneDown); FlowInPropyleneDown_item.Close();
            FlowRightEmpty_item = GetItem(FieldName.FlowRightEmpty); FlowRightEmpty_item.Close();

            //label
            LabelPressure_item = GetItem(FieldName.LabelPressure); LabelPressure_item.Write(_pressure_value);
            LabelTemperature_item = GetItem(FieldName.LabelTemperature); LabelTemperature_item.Write(_temperature_value);
            LabelFlow_item = GetItem(FieldName.LabelFlow); LabelFlow_item.Write(0);
            LabelContent_item = GetItem(FieldName.LabelContent); LabelContent_item.Write(0);
            LabelH2press_item = GetItem(FieldName.LabelH2press); LabelH2press_item.Write(_total_pressure_hopper);
            LabelWeight1_item = GetItem(FieldName.LabelWeight1); LabelWeight1_item.Write(_total_weight1);
            LabelWeight2_item = GetItem(FieldName.LabelWeight2); LabelWeight2_item.Write(_total_weight1);
            LabelPressureHopper_item = GetItem(FieldName.LabelPressureHopper); LabelPressureHopper_item.Write(_total_pressure_hopper);//活化剂压力

            //闪烁
            FlashKettle_item = GetItem(FieldName.FlashKettle); FlashKettle_item.StopFlow();
            FlashPressure_item = GetItem(FieldName.FlashPressure); FlashPressure_item.Close();
            FlashTemperature_item = GetItem(FieldName.FlashTempreture); FlashTemperature_item.Close();
            FlashHopperPressure_item = GetItem(FieldName.FlashHopperPressure); FlashHopperPressure_item.Close();
            FlashWarning_item = GetItem(FieldName.FlashWarning); FlashWarning_item.Close();
        }
        #endregion

        #region 监听步骤，计分
        private void ListenSerialPort()
        {
            string chkcmd_1 = _flowcommand.Items.SingleOrDefault(i => i.Name == "chkcmd1").Value;  //"#RYW11651&";
            string chkcmd_2 = _flowcommand.Items.SingleOrDefault(i => i.Name == "chkcmd2").Value;//"#RYW11652&";
            string chkcmd_3 = _flowcommand.Items.SingleOrDefault(i => i.Name == "chkcmd3").Value;//"#RYW11653&";

            Task listSerialPortState = new Task(
                () =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (_serialPort.IsOpen)
                        {
                            lock (this)
                            {
                                Thread.Sleep(150);
                                try
                                {
                                    _serialPort.Write(chkcmd_1);
                                }
                                catch (Exception)
                                {}
                                
                            }
                        }
                        
                        if (_serialPort.IsOpen)
                        {
                            lock (this)
                            {
                                Thread.Sleep(150);
                                try
                                {
                                    _serialPort.Write(chkcmd_2);
                                }
                                catch (Exception) { }
                            }
                        }
                        
                        if (_serialPort.IsOpen)
                        {
                            lock (this)
                            {
                                Thread.Sleep(150);
                                try
                                {
                                    _serialPort.Write(chkcmd_3);
                                }
                                catch (Exception)
                                {}
                            }
                        }
                    }
                });
            listSerialPortState.Start();
        }


        int _replace_count = 1;//丙烯置换次数
        bool _can_open_empty = false;
        int _propylene_number = 1;//左侧充丙烯次数
        bool _can_fill_second_propylene = false;
        bool _can_fill_propylene = false;
        private void ListenSignal()
        {
            Task t = new Task(() =>
            {
                while (!_listenCts.IsCancellationRequested)
                {
                    int ValveLeftEmpty = Convert.ToInt32(ReadItem(FieldName.ValveLeftEmpty));
                    int ValveOutO2 = Convert.ToInt32(ReadItem(FieldName.ValveOutO2));
                    int ValveInActivator = Convert.ToInt32(ReadItem(FieldName.ValveInActivator));
                    int ValveInCold = Convert.ToInt32(ReadItem(FieldName.ValveInCold));
                    int ValveInColdSecond = Convert.ToInt32(ReadItem(FieldName.ValveInColdSecond));
                    int ValveInGas = Convert.ToInt32(ReadItem(FieldName.ValveInGas));
                    int ValveInH2 = Convert.ToInt32(ReadItem(FieldName.ValveInH2));
                    int ValveInHot = Convert.ToInt32(ReadItem(FieldName.ValveInHot));
                    int ValveInN2 = Convert.ToInt32(ReadItem(FieldName.ValveInN2Right));
                    int ValveInN2Left = Convert.ToInt32(ReadItem(FieldName.ValveInN2Left));
                    int ValveInPropylene = Convert.ToInt32(ReadItem(FieldName.ValveInPropyleneLeft));
                    int ValveInPropyleneRight = Convert.ToInt32(ReadItem(FieldName.ValveInPropyleneRight));
                    int ValveLeftHopper = Convert.ToInt32(ReadItem(FieldName.ValveLeftHopper));
                    int ValveOutCold = Convert.ToInt32(ReadItem(FieldName.ValveOutCold));
                    int ValveOutGas = Convert.ToInt32(ReadItem(FieldName.ValveOutGas));
                    int ValveOutHot = Convert.ToInt32(ReadItem(FieldName.ValveOutHot));
                    int ValveOutPropylene = Convert.ToInt32(ReadItem(FieldName.ValveOutPropylene));
                    int ValveOutPropyleneWithHopper = Convert.ToInt32(ReadItem(FieldName.ValveOutPropyleneWithHopper));
                    int ValveRightHopper = Convert.ToInt32(ReadItem(FieldName.ValveRightHopper));
                    int ValveInDownPropylene = Convert.ToInt32(ReadItem(FieldName.ValveInDownPropylene));
                    int ValveEmptyFromHopper = Convert.ToInt32(ReadItem(FieldName.ValveEmptyFromHopper));
                    int ValveStir = Convert.ToInt32(ReadItem(FieldName.ValveStir));
                    int ValveStop = Convert.ToInt32(ReadItem(FieldName.ValveStop));

                    ValveLeftEmpty_State = ValveLeftEmpty == open;
                    ValveOutO2_State = ValveOutO2 == open;
                    ValveInActivator_State = ValveInActivator == open;
                    ValveInCold_State = ValveInCold == open;
                    ValveInColdSecond_State = ValveInColdSecond == open;
                    ValveInGas_State = ValveInGas == open;
                    ValveInH2_State = ValveInH2 == open;
                    ValveInHot_State = ValveInHot == open;
                    ValveInN2Right_State = ValveInN2 == open;
                    ValveInPropyleneLeft_State = ValveInPropylene == open;
                    ValveInPropyleneRight_State = ValveInPropyleneRight == open;
                    ValveLeftHopper_State = ValveLeftHopper == open;
                    //ValveOutCold_State = ValveOutCold == open;
                    ValveOutGas_State = ValveOutGas == open;
                    //ValveOutHot_State = ValveOutHot == open;
                    ValveOutPropylene_State = ValveOutPropylene == open;
                    ValveOutPropyleneWithHopper_State = ValveOutPropyleneWithHopper == open;
                    ValveRightHopper_State = ValveRightHopper == open;
                    //ValveInDownPropylene_State = ValveInDownPropylene == open;
                    //ValveEmptyFromHopper_State = ValveEmptyFromHopper == open;
                    ValveStir_State = ValveStir == open;
                    ValveStop_State = ValveStop == open;

                    DateTime now = DateTime.Now;
                    Step step = new Step()
                    {
                        Status = new Dictionary<Enum,double>()
                        {
                                {StatusName.丙烯总量,ValveInGas},
                                {StatusName.反应釜压力,_pressure_value},
                                {StatusName.冷水阀辅 , ValveInColdSecond},
                                {StatusName.冷水阀主,ValveInCold},
                                {StatusName.冷水回水阀,ValveOutCold},
                                {StatusName.气相丙烯阀1,-1},
                                {StatusName.气相丙烯阀2,-1},
                                {StatusName.气相丙烯阀3,-1},
                                {StatusName.氢气阀,ValveInH2},
                                {StatusName.热水阀,ValveInHot},
                                {StatusName.热水回水阀,ValveOutHot},
                                {StatusName.温度,_temperature_value},

                                {StatusName.右侧丙烯重量, _weight_right},
                                {StatusName.右侧氮气阀,ValveInN2},
                                {StatusName.右侧放空总管1,-1},
                                {StatusName.右侧放空总管2,-1},
                                {StatusName.右侧放空总管3,-1},
                                {StatusName.右侧精致丙烯总管,ValveInPropyleneRight},
                                {StatusName.右侧去丙烯回收系统,ValveOutPropylene},
                                {StatusName.右侧去丙烯回收系统从料斗, ValveEmptyFromHopper},
                                {StatusName.右侧去高点放空总管,ValveOutO2},
                                {StatusName.右侧料斗下料阀,ValveRightHopper},

                                {StatusName.左侧丙烯重量1,-1},
                                {StatusName.左侧丙烯重量2,-1},
                                {StatusName.左侧氮气阀,ValveInN2Left},
                                {StatusName.左侧放空阀,ValveLeftEmpty},
                                {StatusName.左侧活化剂进料阀,ValveInActivator},
                                {StatusName.左侧活化剂料斗阀,ValveLeftHopper},
                                {StatusName.左侧活化剂压力,_pressure_hopper},
                                {StatusName.左侧精致丙烯进料阀,ValveInPropylene},
                                {StatusName.左侧气相丙烯回收阀,ValveOutGas},
                                {StatusName.左侧氢气压力,_pressure_h2},
                                {StatusName.搅拌开关,ValveStir},
                                 {StatusName.急停按钮,ValveStop},
                        },
                        Date = now
                    };

                    #region 计算置换
                    switch (_replace_count)
                    {
                        case 1:
                            step.Status[StatusName.气相丙烯阀1] = ValveInGas;
                            if (_can_open_empty)
                            {
                                if (_pressure_value == 0 && ValveOutO2 == open)
                                {
                                    step.Status[StatusName.右侧放空总管1] = ValveOutO2;
                                    _replace_count = 2;
                                    _can_open_empty = false;
                                }
                            }
                            break;
                        
                    }

                    switch (_propylene_number)
                    {
                        case 1:
                            step.Status[StatusName.左侧丙烯重量1] = _weight_left1;
                            if (_can_fill_second_propylene)
                            {
                                _propylene_number = 2;
                                _can_fill_propylene = false;
                            }
                            break;
                       
                    }

                    #endregion
                    _steps.Add(step);
                    Thread.Sleep(_sleepfast);
                }
            });
            t.Start();
        }

        int valveLeftEmpty = -1,
                        valveOutO2 = -1,
                        valveInActivator = -1,
                        valveInCold = -1,
                        valveInColdSecond = -1,
                        valveInGas = -1,
                        valveInH2 = -1,
                        valveInHot = -1,
                        valveInN2Right = -1,
                        valveInN2Left = -1,
                        valveInPropyleneLeft = -1,
                        valveInPropyleneRight = -1,
                        valveLeftHopper = -1,
                        valveOutGas = -1,
                        valveOutPropylene = -1,
                        valveOutPropyleneWithHopper = -1,
                        valveRightHopper = -1,
                        valveEmptyFromHopper = -1,
                        valveStir = -1,
                        valveStop = -1;
        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int n = _serialPort.BytesToRead;
            byte[] data = new byte[n];
            _serialPort.Read(data, 0, data.Length);

            string temp = Encoding.Default.GetString(data);
            if (temp.Contains("\n"))
            {
                Match match = Regex.Match(temp, @"\d{5}_\d{3}");
                if (match.Success)
                {
                    string s = match.Groups[0].Value.Split('_')[1];
                    int v = Convert.ToInt32(s);
                    string r = Convert.ToString(v, 2).PadLeft(8,'0');
                    string prefix = match.Groups[0].Value.Split('_')[0];


                    if (prefix == _prefix1)
                    {
                        valveOutPropylene = Convert.ToInt32(r[0]);
                        valveInGas = Convert.ToInt32(r[1]);
                        valveStop = Convert.ToInt32(r[2]);
                        valveStir = Convert.ToInt32(r[3]);
                        valveEmptyFromHopper = Convert.ToInt32(r[4]);//todo??
                        valveOutGas = Convert.ToInt32(r[5]);
                        valveInActivator = Convert.ToInt32(r[6]);
                        valveOutO2 = Convert.ToInt32(r[7]);//todo??

                        ValveOutPropylene_State = valveOutPropylene == open;
                        ValveInGas_State = valveInGas == open;
                        ValveStop_State = valveStop == open;
                        ValveStir_State = valveStir == open;
                        //ValveEmptyFromHopper_State = valveEmptyFromHopper == open;
                        ValveOutGas_State = valveOutGas == open;
                        ValveInActivator_State = valveInActivator == open;
                        ValveOutO2_State = valveOutO2 == open;

                        _isgetoverstate1 = true;
                    }
                    else if (prefix == _prefix2)
                    {
                        valveInPropyleneRight = Convert.ToInt32(r[0]);
                        valveLeftEmpty = Convert.ToInt32(r[2]);
                        valveInPropyleneLeft = Convert.ToInt32(r[3]);
                        valveInHot = Convert.ToInt32(r[5]);

                        ValveInPropyleneRight_State = valveInPropyleneRight == open;
                        ValveLeftEmpty_State = valveLeftEmpty == open;
                        ValveInPropyleneLeft_State = valveInPropyleneLeft == open;
                        ValveInHot_State = valveInHot == open;
                        _isgetoverstate2 = true;
                    }
                    else if (prefix == _prefix3)
                    {
                        valveInH2 = Convert.ToInt32(r[0]);
                        valveRightHopper = Convert.ToInt32(r[1]);
                        valveOutPropyleneWithHopper = Convert.ToInt32(r[2]);
                        valveLeftHopper = Convert.ToInt32(r[3]);
                        valveInN2Right = Convert.ToInt32(r[4]);
                        valveInCold = Convert.ToInt32(r[5]);
                        valveInColdSecond = Convert.ToInt32(r[6]);
                        valveInN2Left = Convert.ToInt32(r[7]);

                        ValveInH2_State = valveInH2 == open;
                        ValveRightHopper_State = valveRightHopper == open;
                        ValveOutPropyleneWithHopper_State = valveOutPropyleneWithHopper == open;
                        ValveLeftHopper_State = valveLeftHopper == open;
                        ValveInN2Right_State = valveInN2Right == open;
                        ValveInCold_State = valveInCold == open;
                        ValveInColdSecond_State = valveInColdSecond == open;

                        _isgetoverstate3 = true;
                    }


                    #region 添加步骤
                    DateTime now = DateTime.Now;
                    Step step = new Step()
                    {
                        Status = new Dictionary<Enum, double>()
                        {
                                {StatusName.丙烯总量,valveInGas},
                                {StatusName.反应釜压力,_pressure_value},
                                {StatusName.冷水阀辅 , valveInColdSecond},
                                {StatusName.冷水阀主,valveInCold},
                                //{StatusName.冷水回水阀,valveOutCold},
                                {StatusName.气相丙烯阀1,-1},
                                {StatusName.气相丙烯阀2,-1},
                                {StatusName.气相丙烯阀3,-1},
                                {StatusName.氢气阀,valveInH2},
                                {StatusName.热水阀,valveInHot},
                                //{StatusName.热水回水阀,valveOutHot},
                                {StatusName.温度,_temperature_value},

                                {StatusName.右侧丙烯重量, _weight_right},
                                {StatusName.右侧氮气阀,valveInN2Right},
                                {StatusName.右侧放空总管1,-1},
                                {StatusName.右侧放空总管2,-1},
                                {StatusName.右侧放空总管3,-1},
                                {StatusName.右侧精致丙烯总管,valveInPropyleneRight},
                                {StatusName.右侧去丙烯回收系统,valveOutPropylene},
                                {StatusName.右侧去丙烯回收系统从料斗, valveEmptyFromHopper},
                                {StatusName.右侧去高点放空总管,valveOutO2},
                                {StatusName.右侧料斗下料阀,valveRightHopper},

                                {StatusName.左侧丙烯重量1,-1},
                                {StatusName.左侧丙烯重量2,-1},
                                {StatusName.左侧氮气阀,valveInN2Left},
                                {StatusName.左侧放空阀,valveLeftEmpty},
                                {StatusName.左侧活化剂进料阀,valveInActivator},
                                {StatusName.左侧活化剂料斗阀,valveLeftHopper},
                                {StatusName.左侧活化剂压力,_pressure_hopper},
                                {StatusName.左侧精致丙烯进料阀,valveInPropyleneLeft},
                                {StatusName.左侧气相丙烯回收阀,valveOutGas},
                                {StatusName.左侧氢气压力,_pressure_h2},
                                {StatusName.搅拌开关,valveStir},
                                 {StatusName.急停按钮,valveStop},
                        },
                        Date = now
                    };

                    #region 计算置换
                    switch (_replace_count)
                    {
                        case 1:
                            step.Status[StatusName.气相丙烯阀1] = valveInGas;
                            if (_can_open_empty)
                            {
                                if (_pressure_value == 0 && valveOutO2 == open)
                                {
                                    step.Status[StatusName.右侧放空总管1] = valveOutO2;
                                    _replace_count = 2;
                                    _can_open_empty = false;
                                }
                            }
                            break;
                        
                    }

                    switch (_propylene_number)
                    {
                        case 1:
                            step.Status[StatusName.左侧丙烯重量1] = _weight_left1;
                            if (_can_fill_second_propylene)
                            {
                                _propylene_number = 2;
                                _can_fill_propylene = false;
                            }
                            break;
                       
                    }

                    #endregion
                    _steps.Add(step); 
                    #endregion
                }
            }
        }


        private void ListenOPCRelation()
        {
            //温度与压力的比例
            double rate = 3.6 / (77 - 22);
            ThreadPool.QueueUserWorkItem((o) =>
            {
                #region 左侧 精致丙烯进料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInPropyleneLeft_State)
                        {
                            if (!Isinner) ValveInPropyleneLeft_item.Open();
                            FlowInPropylene_item.StartFlow();

                            if (ValveLeftHopper_State && _total_weight1 > 0)
                            {
                                _total_weight1 -= _flow_value;
                                LabelWeight1_item.Write(_total_weight1);//剩余量                                
                                SpHeightValue(1,_total_weight1);
                                if (_can_fill_second_propylene)
                                {
                                    _weight_left2 += _flow_value;
                                }
                                else
                                {
                                    _weight_left1 += _flow_value;
                                }
                            }
                        }
                        else
                        {
                            if (_weight_left2 > 3000)
                                _can_fill_propylene = true;

                            if (!Isinner) ValveInPropyleneLeft_item.Close();
                            FlowInPropylene_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 左侧 活化物 打开 活化剂压力下降
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInActivator_State && ValveLeftHopper_State)
                        {
                            if (!Isinner) { 
                                ValveInActivator_item.Open();
                                ValveLeftHopper_item.Open();
                            }
                            _total_pressure_hopper -= _pressure_hopper_crement;

                            if (_total_pressure_hopper > 0)
                            {
                                LabelPressureHopper_item.Write(_total_pressure_hopper);

                                _pressure_hopper += _pressure_hopper_crement;
                                _pressure_value += _pressure_hopper_crement;


                                if (_pressure_hopper > 0.1)
                                {
                                    _can_fill_second_propylene = true;
                                }

                                if (_pressure_value > 0.2)
                                {
                                    BeginWarning(WarningType.活化剂压力过高);
                                }
                                LabelPressure_item.Write(_pressure_value);
                                SpPressureValue(_pressure_value);
                            }
                            else
                            {
                                _total_pressure_hopper = 0;
                            }
                            FlowInActivator_item.StartFlow();
                        }
                        else
                        {
                            StopWarning(WarningType.活化剂压力过高);
                            if (!Isinner) ValveInActivator_item.Close();
                            FlowInActivator_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });

                #endregion

                #region 左侧 加料斗
                //Task.Run(() =>
                // {
                //     while (!_listenCts.IsCancellationRequested)
                //     {
                //         if (ValveLeftHopper_State)
                //         {
                //             //ValveLeftHopper_item.Open();
                //             FlowInLeft_item.StartFlow();
                //         }
                //         else
                //         {
                //             //ValveLeftHopper_item.Close();
                //             FlowInLeft_item.StopFlow();
                //         }
                //         Thread.Sleep(_sleepfast);
                //     }
                // });
                #endregion

                #region 左侧 气相丙烯回收
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveOutGas_State)
                        {
                            if (!Isinner) ValveOutGas_item.Open();
                            FlowOutGas_item.StartFlow();
                        }
                        else
                        {
                            if (!Isinner) ValveOutGas_item.Close();
                            FlowOutGas_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }

                });
                #endregion

                #region 左侧 放空管道
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveLeftEmpty_State)
                        {
                            _pressure_decrement = _pressure_value < 0.05 ? 0.01 : 0.04;
                            if (!Isinner) ValveLeftEmpty_item.Open();
                            FlowLeftEmpty_item.StartFlow();

                            if (ValveLeftHopper_State)
                            {
                                _pressure_value-=_pressure_decrement;
                                if (_pressure_value > 0)
                                {
                                    LabelPressure_item.Write(_pressure_value);
                                    SpPressureValue(_pressure_value);
                                }
                                else
                                {
                                    _pressure_value = 0;
                                    LabelPressure_item.Write(_pressure_value);
                                }
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveLeftEmpty_item.Close();
                            FlowLeftEmpty_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion
               
                #region 左侧 气相丙烯 1
                Task.Run(() => {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInGas_State)
                        {
                            _pressure_increment = 0.02;
                            if (!Isinner) ValveInGas_item.Open();
                            FlowInGas_item.StartFlow();
                            _pressure_value += _pressure_increment;
                            LabelPressure_item.Write(_pressure_value);
                            SpPressureValue(_pressure_value);
                            if (_pressure_value > 1.5)
                                BeginWarning(WarningType.气相丙烯压力过高);

                            if (_pressure_value>0.5)
                            {
                                _can_open_empty = true;
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveInGas_item.Close();
                            StopWarning(WarningType.气相丙烯压力过高);
                            FlowInGas_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 左侧 氢气 1
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInH2_State)
                        {
                            _pressure_increment = 0.02;
                            if (!Isinner) ValveInH2_item.Open();
                            FlowInH2_item.StartFlow();
                            _total_pressure_h2 -= _pressure_increment;
                            _h2_flow += _h2_flow_crement;

                            if (_total_pressure_h2 > 0)
                            {
                                LabelH2press_item.Write(_total_pressure_h2);
                                _pressure_value += _pressure_increment;
                                LabelPressure_item.Write(_pressure_value);
                                SpPressureValue(_pressure_value);


                                SpFlowValue(_h2_flow);

                            }

                            if (_pressure_value > 0.15)
                            {
                                BeginWarning(WarningType.氢气压力过高);
                            }
                        }
                        else
                        {
                            StopWarning(WarningType.氢气压力过高);
                            if (!Isinner) ValveInH2_item.Close();
                            FlowInH2_item.StopFlow();
                            SpFlowValue(0);
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion


                #region 右侧 放空总管 +
                Task.Run(() => {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveOutO2_State)
                        {
                            if (!Isinner) ValveOutO2_item.Open();
                            FlowOutO2_item.StartFlow();

                            _pressure_value = Math.Round(_pressure_value, 2);
                            _pressure_value -= _pressure_decrement;

                            _pressure_decrement = _pressure_value < 0.05 ? 0.01 : 0.04;


                            if (_pressure_value >= 0.01)
                            {
                                LabelPressure_item.Write(_pressure_value);
                                SpPressureValue(_pressure_value);
                            }
                            else
                            {
                                _pressure_value = 0;
                                LabelPressure_item.Write(_pressure_value);
                                FlashPressure_item.Close();
                            }
                        }
                        else
                        {
                            if (!Isinner) ValveOutO2_item.Close();
                            FlowOutO2_item.StopFlow();
                            FlashPressure_item.Close();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 右侧 氮气阀
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInN2Right_State)
                        {
                            _pressure_increment = 0.01;
                            if (!Isinner) ValveInN2Right_item.Open();
                            FlowInN2_item.StartFlow();
                            if (ValveRightHopper_State)
                            {
                                _pressure_value += _pressure_increment;
                                LabelPressure_item.Write(_pressure_value);
                                SpPressureValue(_pressure_value);
                            }
                            
                        }
                        else
                        {
                            if (!Isinner) ValveInN2Right_item.Close();
                            FlowInN2_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion


                #region 右侧 精致丙烯进料
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveInPropyleneRight_State)
                        {
                            if (!Isinner) ValveInPropyleneRight_item.Open();
                            FlowInPropyleneRight_item.StartFlow();

                            if(ValveRightHopper_State && _total_weight2>0)
                            {                                
                                _total_weight2 -= _flow_value;

                                if (_total_weight2 > 0)
                                {
                                    LabelWeight2_item.Write(_total_weight2);
                                    _weight_right += _flow_value;
                                    SpHeightValue(4, _total_weight2);
                                }
                                

                            }
                        }
                        else
                        {
                            if (!Isinner) ValveInPropyleneRight_item.Close();
                            FlowInPropyleneRight_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                

                #region 右侧 去丙烯回收
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveOutPropyleneWithHopper_State)
                        {
                            FlowOutPropylene_item.StartFlow();
                        }
                        else
                        {
                            FlowOutPropylene_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 右侧 加料斗
                Task.Run(() =>
                {
                    //while (!_listenCts.IsCancellationRequested)
                    //{
                    //    if (ValveRightHopper_State)
                    //    {
                    //        //FlowInRight_item.StartFlow();
                    //    }
                    //    else
                    //    {
                    //        //FlowInRight_item.StopFlow();
                    //    }
                    //    Thread.Sleep(_sleepfast);
                    //}
                });
                #endregion

                #region 右侧 去丙烯回收系统 从反应釜 黄色管道
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (ValveOutPropylene_State)
                        {
                            if (!Isinner) ValveOutPropylene_item.Open();
                            FlowOutGasFomKettle_item.Open();
                             
                        }
                        else
                        {
                            if (!Isinner) ValveOutPropylene_item.Close();
                            FlowOutGasFomKettle_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 冷水主

                ColdInHelper();
                #endregion

                #region 冷水辅
                ColdInHelper();
                #endregion

                #region 热水阀
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        
                        if (ValveInHot_State)
                        {
                            if (!Isinner) ValveInHot_item.Open();
                            FlowInHot_item.StartFlow();
                            _temperature_value += _temperature_increment;
                            _pressure_value += _pressure_increment;
                            
                            if (_temperature_value > 80)
                            {
                                _temperature_increment = 0.1;
                                _pressure_increment = rate * _temperature_increment;
                                
                            }
                            else if (_temperature_value > 70)
                            {
                                _temperature_increment = 0.2;
                                _pressure_increment = rate * _temperature_increment;
                                
                            }
                            else if (_temperature_value > 60)
                            { 
                                _temperature_increment = 1;
                                _pressure_increment = rate * _temperature_increment;
                            }
                            else
                            { 
                                _temperature_increment = 2;
                                _pressure_increment = rate * _temperature_increment;
                            }

                            if (_pressure_value > 3.8)
                            {
                                _pressure_increment = 0;
                            }

                          

                            if (_temperature_value > 90)
                            {
                                _temperature_increment = 0;
                                _pressure_increment = 0;
                                FlashTemperature_item.Open();
                                BeginWarning(WarningType.反应温度过高);
                            }
                            else
                            {
                                FlashTemperature_item.Close();
                                StopWarning(WarningType.反应温度过高);
                            }

                            LabelPressure_item.Write(_pressure_value);
                            LabelTemperature_item.Write(_temperature_value);
                            SpPressureValue(_pressure_value);
                            SpTemperatureValue(_temperature_value);
                        }
                        else
                        {
                            if (_temperature_value < 80)
                            {
                                FlashTemperature_item.Close();
                                StopWarning(WarningType.反应温度过高);
                            }
                               
                            if (!Isinner) 
                                ValveInHot_item.Close();
                            FlowInHot_item.StopFlow();
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion

                #region 不再用
                #region 冷水出
                //Task.Run(() =>
                //{
                //    while (!_listenCts.IsCancellationRequested)
                //    {
                //        if (ValveOutCold_State)
                //        {
                //            if (!Isinner) ValveOutCold_item.Open();
                //            FlowOutCold_item.StartFlow();
                //        }
                //        else
                //        {
                //            if (!Isinner) ValveOutCold_item.Close();
                //            FlowOutCold_item.StopFlow();
                //        }
                //        Thread.Sleep(_sleepfast);
                //    }
                //});
                #endregion

                #region 热水出
                //Task.Run(() =>
                //{
                //    while (!_listenCts.IsCancellationRequested)
                //    {
                //        if (ValveOutHot_State)
                //        {
                //            if (!Isinner) ValveOutHot_item.Open();
                //            FlowOutHot_item.StartFlow();
                //        }
                //        else
                //        {
                //            if (!Isinner) ValveOutHot_item.Close();
                //            FlowOutHot_item.StopFlow();
                //        }
                //        Thread.Sleep(_sleepfast);
                //    }
                //});
                #endregion
                #region 右侧 去高点放空阀
                //Task.Run(() =>
                //{
                //    while (!_listenCts.IsCancellationRequested)
                //    {
                //        if (ValveEmptyFromHopper_State)
                //        {
                //            _pressure_decrement = _pressure_value < 0.05 ? 0.01 : 0.04;
                //            if (!Isinner) ValveEmptyFromHopper_item.Open();
                //            FlowRightEmpty_item.Open();
                //            if (ValveRightHopper_State && _pressure_value>0)
                //            {
                //                _pressure_value -= _pressure_decrement;
                //                LabelPressure_item.Write(_pressure_value);
                //                SpPressureValue(_pressure_value);
                //            }

                //        }
                //        else
                //        {
                //            if (!Isinner) ValveEmptyFromHopper_item.Close();
                //            FlowRightEmpty_item.StopFlow();
                //        }
                //        Thread.Sleep(_sleepfast);
                //    }
                //});
                #endregion

                #region 左侧 氮气 （不用了）
                //Task.Run(() => {
                //    while (!_listenCts.IsCancellationRequested)
                //    {
                //        if (ValveInN2Left_State)
                //        {
                //            if (!Isinner) ValveInN2Left_item.Open();
                //            FlowInN2Left_item.StartFlow();
                //            if (ValveLeftHopper_State && ValveInActivator_State)
                //            {
                //                _pressure_value += _pressure_increment;
                //                LabelPressure_item.Write(_pressure_value);
                //            }

                //            if (_pressure_value > 0.1)
                //            {
                //                _can_fill_second_propylene = true;
                //            }
                //        }
                //        else
                //        {
                //            if (!Isinner) ValveInN2Left_item.StopFlow();
                //            FlowInN2Left_item.StopFlow();
                //        }
                //        Thread.Sleep(_sleepfast);
                //    }
                //});

                #endregion 
                #endregion

                #region 搅拌釜状态
                Task.Run(() =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (!ValveStir_State)
                        {
                            StopStir();
                        }
                        else
                        {
                            BeginStir();

                            FlashKettle_item.Open();
                            Thread.Sleep(300);
                            FlashKettle_item.Close();
                            Thread.Sleep(300);
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
                #endregion
            });
        }
        #endregion

        #region 处理异常的方法
        #region 温度异常

        protected override void BeginTempretureAbnormal(double limit)
        {
            _warningDicionary.Add(WarningType.温度异常, false);
            Task.Run(() =>
            {
                while (_temperature_value < limit)
                {
                    _temperature_value += 5;
                    LabelTemperature_item.Write(_temperature_value);
                    SpTemperatureValue(_temperature_value);
                    Thread.Sleep(_sleepfast);
                }
                BeginWarning(WarningType.温度异常); 
                FlashTemperature_item.StartFlow();
                HandleTempretureAbnormal();
            });
        }

        private void HandleTempretureAbnormal()
        {
            Task.Run(
               () =>
               {
                   Parallel.Invoke(CloseHotValve, OpenColdValve);
                   StopWarning(WarningType.温度异常);
                   _warningDicionary[WarningType.温度异常] = true;
               });
        }

        private void OpenColdValve()
        {
            while (!_listenCts.IsCancellationRequested)
            {
                if (_temperature_value <= 50)
                {
                    AddCompose("处理温度异常：打开冷水阀");
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
        }

        private void CloseHotValve()
        {
            while (!_listenCts.IsCancellationRequested)
            {
                if (!ValveInHot_State)
                {
                    AddCompose("处理温度异常：关闭热水阀");
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
        }
        #endregion

        #region 压力异常

        protected override void BeginPressureAbnormal(double value)
        {
            _warningDicionary.Add(WarningType.压力异常, false);
            Task.Run(() =>
            {
                while (_pressure_value < value)
                {
                    _pressure_value += 0.1;
                    LabelPressure_item.Write(_pressure_value);
                    SpPressureValue(_pressure_value);
                    Thread.Sleep(_sleepfast);
                }
                BeginWarning(WarningType.压力异常);
                FlashPressure_item.StartFlow();
                HandlePressureAbnormal();
            });
        }

        private void HandlePressureAbnormal()
        {
            Task.Run(
              () =>
              {
                  Parallel.Invoke(OpenAirValve, CloseHotValve4Pressure);
                  StopWarning(WarningType.压力异常);
                  _warningDicionary[WarningType.压力异常] = true;
              });
        }

        private void CloseHotValve4Pressure()
        {
            while (!_listenCts.IsCancellationRequested)
            {
                if (!ValveInHot_State)
                {
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
        }

        private void OpenAirValve()
        {
            _pressure_decrement = 0.02;//加快降压
            //打开放空阀
            while (!_listenCts.IsCancellationRequested)
            {
                if (ValveOutO2_State)
                {
                    if (_pressure_value <= 0.01)
                    {
                        break;
                    }
                }
                Thread.Sleep(_sleepfast);
            }
        }
        #endregion

        #region 泄漏

        public override void LeakageAbnormal()
        {
            BeginLeakAbnormal();
        }

        protected override void BeginLeakAbnormal()
        {
            _isleakAbnormal = false;
            BeginWarning(WarningType.泄漏);
            BeginLeak();
            _warningDicionary.Add(WarningType.泄漏, false);
            HandleLeakAbnormal();
        }

        private void HandleLeakAbnormal()
        {
            //打开急停按钮
            while (!_listenCts.IsCancellationRequested)
            {
                if (ValveStop_State)
                {
                    AddCompose("处理泄漏异常：按下急停按钮");
                    break;
                }
                Thread.Sleep(_sleepfast);
            }
            Parallel.Invoke(
                () =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (!ValveInHot_State )
                        {
                            AddCompose("处理泄漏异常：关闭热水阀");
                            break;
                        }
                        Thread.Sleep(_sleepfast);
                    }
                },
                () =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (!ValveInCold_State && !ValveInColdSecond_State)
                        {
                            AddCompose("处理泄漏异常：关闭冷水阀");
                            break;
                        }
                        Thread.Sleep(_sleepfast);
                    }
                },
                () =>
                {
                    while (!_listenCts.IsCancellationRequested)
                    {
                        if (!ValveInPropyleneLeft_State && !ValveInPropyleneRight_State)
                        {
                            AddCompose("处理泄漏异常：关闭丙烯进料阀");
                            break;
                        }
                        Thread.Sleep(_sleepfast);
                    }
                });
            StopWarning(WarningType.泄漏);
            _warningDicionary[WarningType.泄漏] = true;
        }

        #endregion
        #endregion

        #region 重写公共方法
        public override void Begin()
        {
            ListenOPCRelation();//联动
            CheckAbnormalPoint();
        }

        public override async Task<string> CheckDefault()
        {
            StringBuilder builder = new StringBuilder();
           await Task.Run(() =>
            {
                if (!Isinner)
                {
                    while (!_isgetoverstate1 || !_isgetoverstate2 || !_isgetoverstate3)
                    {
                        Thread.Sleep(_sleepfast);
                    }
                }
                

                if (ValveLeftEmpty_State)
                    builder.AppendLine("左侧放空阀未关闭");
                if (ValveOutO2_State)
                    builder.AppendLine("放空总阀未关闭");
                if (ValveInActivator_State)
                    builder.AppendLine("活化剂阀门未关闭");
                if (ValveInCold_State)
                    builder.AppendLine("冷水阀（主）未关闭");
                if (ValveInColdSecond_State)
                    builder.AppendLine("冷水阀（辅）未关闭");
                if (ValveInGas_State)
                    builder.AppendLine("气相丙烯进料阀未关闭");
                if (ValveInH2_State)
                    builder.AppendLine("氢气阀未关闭");
                if (ValveInHot_State)
                    builder.AppendLine("热水阀未关闭");
                if (ValveInN2Right_State)
                    builder.AppendLine("右侧氮气阀未关闭");
                if (ValveInPropyleneLeft_State)
                    builder.AppendLine("左侧丙烯进料阀未闭关");
                if (ValveInPropyleneRight_State)
                    builder.AppendLine("右侧丙烯进料阀未关闭");
                if (ValveLeftHopper_State)
                    builder.AppendLine("左侧活化剂料斗阀门未关闭");
                if (ValveOutGas_State)
                    builder.AppendLine("气相丙烯回收阀未关闭");
                if (ValveOutPropylene_State)
                    builder.AppendLine("丙烯回收阀未关闭");
                if (ValveOutPropyleneWithHopper_State)
                    builder.AppendLine("右侧去丙烯回收阀未关闭");
                if (ValveRightHopper_State)
                    builder.AppendLine("右侧催化剂料斗阀未关闭");
                //if (ValveEmptyFromHopper_State)
                //    builder.AppendLine("右侧去高点放空阀未关闭");
                if (ValveStir_State)
                    builder.AppendLine("搅拌按钮未关闭");
                if (ValveStop_State)
                    builder.AppendLine("急停按钮未关闭");

                
            });

           return builder.ToString();
        }
        public override void Over()
        {
            Close();
            CalcFlowAndStep();
            Score = CalcScore();
            CloseAllRelay();
            Finish();
        }

        public override void Close()
        {
            this._listenCts.Cancel();
            base.Close();
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= _serialPort_DataReceived;
                _serialPort.Close();
            }
        }

        void CalcFlowAndStep()
        {
            #region 计算流程
            //丙烯置换 - 气相丙烯压力1
            var gas1_in_item = _steps.Where(i => i.Status[StatusName.气相丙烯阀1] == open)
                .OrderByDescending(i => i.Status[StatusName.反应釜压力]).OrderByDescending(i => i.Date).FirstOrDefault();

            //放空压力1
            //var empty1_out_item = gas1_in_item != null ? _steps.Where(i => i.Status[StatusName.右侧放空总管1] == open && i.Date > gas1_in_item.Date)
            //    .OrderBy(i => i.Status[StatusName.反应釜压力]).OrderByDescending(i => i.Date).FirstOrDefault() : null;
            var empty1_out_item = gas1_in_item != null ? _steps.Where(i=> i.Date > gas1_in_item.Date)
               .OrderBy(i => i.Status[StatusName.反应釜压力]).OrderByDescending(i => i.Date).FirstOrDefault() : null;

            //开搅拌 - 后面加 xx-xx都需要打开//var stir_count = empty3_out_item != null ? _steps.Where(i => i.Status[StatusName.搅拌开关] == close && i.Date > empty3_out_item.Date).Count() : -1;

            //加氢气
           var h2_in_item = _steps.Where(i => i.Status[StatusName.氢气阀] == open)
                .OrderByDescending(i => i.Status[StatusName.反应釜压力]).ThenByDescending(i => i.Date).FirstOrDefault();

            //左侧放空
            var h2_empty_item = _steps.Where(i => i.Date>h2_in_item.Date)
                .OrderBy(i => i.Status[StatusName.反应釜压力]).ThenByDescending(i => i.Date).FirstOrDefault();

            //左侧 通入丙烯 

            var p1_inleft_count = _steps.Count(i => i.Status[StatusName.左侧精致丙烯进料阀] == open);
            var p1_inleft_maxvalue = p1_inleft_count > 0
                ? _steps.Where(i => i.Status[StatusName.左侧精致丙烯进料阀] == open).Max(i => i.Status[StatusName.左侧丙烯重量1])
                : -1;
            var propylene1_inleft_item = _steps.Where(i => i.Status[StatusName.左侧精致丙烯进料阀] == open
                  && i.Status[StatusName.左侧丙烯重量1] == p1_inleft_maxvalue).OrderBy(i => i.Date).FirstOrDefault();


            //左侧 充氮气
             //通入活化剂0.1-0.2
            var activator_item = _steps.Where(i => i.Status[StatusName.左侧活化剂进料阀] == open)
                .OrderByDescending(i => i.Status[StatusName.左侧活化剂压力]).OrderByDescending(i => i.Date).FirstOrDefault();


            //右侧泄压 不做？？

            //右侧加丙烯 
            //TODO:run可以先做？
            //var propylene_inright_maxvalue = p2_inleft_item != null ? _steps.Where(i => i.Status[StatusName.右侧精致丙烯总管] == open
            //&& i.Date > p2_inleft_item.Date).Max(i => i.Status[StatusName.右侧丙烯重量]) : -1;
            //var propylene_inright_item = propylene_inright_maxvalue != -1 ? _steps.Where(i => i.Status[StatusName.右侧精致丙烯总管] == open
            //&& i.Date > p2_inleft_item.Date && i.Status[StatusName.右侧丙烯重量] == propylene_inright_maxvalue)
            //.OrderByDescending(i => i.Date).FirstOrDefault() : null;

            var propylene_inright_count = _steps.Count(i => i.Status[StatusName.右侧精致丙烯总管] == open);
            var propylene_inright_maxvalue = propylene_inright_count > 0
                ? _steps.Where(i => i.Status[StatusName.右侧精致丙烯总管] == open).Max(i => i.Status[StatusName.右侧丙烯重量])
                : -1;
            var propylene_inright_item = propylene_inright_maxvalue != -1 ?
                _steps.Where(i => i.Status[StatusName.右侧精致丙烯总管] == open
                && i.Status[StatusName.右侧丙烯重量] == propylene_inright_maxvalue)
                .OrderByDescending(i => i.Date).FirstOrDefault() : null;


            //温度平均值 >70开始记录温度
            //var temperature_avg = propylene_inright_item != null?_steps.Where(i=>i.Status[StatusName.温度]>70 
            //    && i.Status[StatusName.热水阀] == open).Average(i => i.Status[StatusName.温度]) : -1;
            var temperature_avg_count = _steps.Count(i => i.Status[StatusName.温度] > 70
                                                          && i.Status[StatusName.热水阀] == open);
            var temperature_avg = temperature_avg_count > 0
                ? _steps.Where(i => i.Status[StatusName.温度] > 70
                                    && i.Status[StatusName.热水阀] == open).Average(i => i.Status[StatusName.温度])
                : -1;

            
            var pressure_avg_count = _steps.Count(i =>
                i.Status[StatusName.温度] > 70
                && i.Status[StatusName.热水阀] == open);
            var pressure_avg = pressure_avg_count > 0 ? _steps.Where(i =>
                 i.Status[StatusName.温度] > 70
                && i.Status[StatusName.热水阀] == open).Average(i => i.Status[StatusName.反应釜压力]) : -1;

            
            //温度最大值
            var hot_temperature_maxvalue_count = _steps.Count(i => i.Status[StatusName.热水阀] == open);
            var hot_temperature_maxvalue = hot_temperature_maxvalue_count > 0
                ? _steps.Where(i => i.Status[StatusName.热水阀] == open).Max(i => i.Status[StatusName.温度])
                : -1;
            var hot_temperature_item = hot_temperature_maxvalue != -1 ? _steps.Where(
                i => i.Status[StatusName.热水阀] == open
                && i.Status[StatusName.温度] == hot_temperature_maxvalue)
                .OrderByDescending(i => i.Date).FirstOrDefault() : null;

            //回收 
            //var recycle_propylene_item = hot_temperature_item != null ? _steps.Where(i => i.Status[StatusName.右侧去丙烯回收系统] == open && i.Date > hot_temperature_item.Date)
            //    .OrderByDescending(i => i.Date).FirstOrDefault() : null;
            var recycle_propylene_item = _steps.Where(i => i.Status[StatusName.右侧去丙烯回收系统] == open)
               .OrderByDescending(i => i.Date).FirstOrDefault();
             
            //回收时 平均温度
            var recycle_temperature_avg_list = recycle_propylene_item != null
                ? _steps.Where(i => i.Status[StatusName.右侧去丙烯回收系统] == open) : null;
            var recycle_temperature_avg = recycle_temperature_avg_list != null ? recycle_temperature_avg_list.Average(i => i.Status[StatusName.温度]) : -1;

            //放空后首次开搅拌的item, 回收前不能关闭
            var stir_firstopen_item =  _steps.FirstOrDefault(i => i.Status[StatusName.搅拌开关] == open );

            #endregion

            #region 计分
            _dicSteps.Clear();
            #region 1 丙烯置换
            //1 第一次丙烯置换  充气相丙烯至(0.5～1.0) MPa置换聚合釜3遍。
            //1.1 充入气相丙烯

            var gas_replace_step = base._points.SingleOrDefault(i => i.Description == "气相丙烯置换");
            if (gas_replace_step.IsChecked)
            {
                if (gas1_in_item != null)
                {
                    var pressure = gas1_in_item.Status[StatusName.反应釜压力];
                    if (pressure > 1.5)
                    {
                        string temp = string.Format("气相丙烯置换-充气相丙烯:反应釜压力:{0}MPa,高于1.5Mpa,压力过高", pressure);
                         _dicSteps.Add("气相丙烯置换-充气相丙烯", new StepDescription() { Description = temp, IsRight = false });
                    }
                    if (pressure >= 0.5 && pressure <= 1.5)
                    {
                        string temp = string.Format("气相丙烯置换-充气相丙烯:反应釜压力:{0}MPa,在0.5Mpa-1.5Mpa范围内", pressure);
                        _dicSteps.Add("气相丙烯置换-充气相丙烯", new StepDescription() { Description = temp, IsRight = true });
                    }
                    else
                    {
                        string temp = string.Format("气相丙烯置换-充气相丙烯:反应釜压力:{0}MPa,低于0.5Mpa,压力不足", pressure);
                        _dicSteps.Add("气相丙烯置换-充气相丙烯", new StepDescription() { Description = temp, IsRight = false });

                    }
                }
                else
                {
                    _dicSteps.Add("气相丙烯置换-充气相丙烯", new StepDescription() { Description = "氮气置换-充氮气，无操作", IsRight = false });
                }

                //1.2 气相丙烯回收
                if (empty1_out_item != null)
                {
                    var pressure = empty1_out_item.Status[StatusName.反应釜压力];
                    if (pressure <= 0.01)
                    {
                        string temp = string.Format("气相丙烯置换-充气相丙烯:打开放空阀，降压至{0}MPa", pressure);
                        _dicSteps.Add("气相丙烯置换-放空", new StepDescription() { Description = temp, IsRight = true });
                    }
                    else
                    {
                        string temp = string.Format("气相丙烯置换-充气相丙烯:打开放空阀，反应釜压力{0}MPa，未降到0.01MPa以下", pressure);
                        _dicSteps.Add("气相丙烯置换-放空", new StepDescription() { Description = temp, IsRight = false });
                    }
                }
                else
                {
                    _dicSteps.Add("气相丙烯置换-放空", new StepDescription() { Description = "气相丙烯置换-充气相丙烯:气相丙烯置换-放空，无操作", IsRight = false });
                }
          
            }
            
            
            #endregion

            //加氢气
            InsertRangeValueStep("加氢", ValueName.压力, h2_in_item, StatusName.反应釜压力);

            //通入丙烯加活化剂
            InsertRangeValueStep("通入丙烯加活化剂",ValueName.重量, propylene1_inleft_item, StatusName.左侧丙烯重量1);

            //通入丙烯加催化剂
            InsertRangeValueStep("通入丙烯加催化剂", ValueName.重量, propylene_inright_item, StatusName.右侧丙烯重量);
   
            //聚合反应温度
            InsertRangeValueStep("聚合反应温度", ValueName.温度, temperature_avg);

            //丙烯回收温度
            InsertRangeValueStep("丙烯回收温度", ValueName.温度, recycle_temperature_avg);

            //开搅拌
            InsertActionStep("开搅拌", stir_firstopen_item);
            
            #endregion

            //处理异常
            #region 温度异常
            var temperature_abnormal_point = base._points.SingleOrDefault(i => i.Description == "温度突变异常");
            if (temperature_abnormal_point!=null&&temperature_abnormal_point.IsChecked)
            {
                var openair_item = _steps.Where(i => i.Status[StatusName.温度] <= 50 && i.Date > _temperatureAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closehot_item = _steps.Where(i => i.Status[StatusName.热水阀] == close && i.Date > _temperatureAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (openair_item != null && closehot_item != null)
                {
                    _dicSteps.Add("处理温度突变异常", new StepDescription() { Description = temperature_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理温度突变异常", new StepDescription() { Description = temperature_abnormal_point.Description + "：未完成", IsRight = false });
                }
            }
            #endregion

             #region 压力异常
            var pressure_abnormal_point = base._points.SingleOrDefault(i => i.Description == "压力突变异常");
            if (pressure_abnormal_point!= null &&pressure_abnormal_point.IsChecked)
            {
                var openair_item = _steps.Where(i => i.Status[StatusName.反应釜压力] <= 0.01 && i.Date > _pressureAbnormalData)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closeh2_item = _steps.Where(i => i.Status[StatusName.热水阀] == close && i.Date > _pressureAbnormalData)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (openair_item != null && closeh2_item != null)
                {
                    _dicSteps.Add("处理压力突变异常", new StepDescription() { Description = pressure_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理压力突变异常", new StepDescription() { Description = pressure_abnormal_point.Description + "：未完成", IsRight = false });
                }
            }
            #endregion

            #region 泄漏异常
            var leak_abnormal_point = base._points.SingleOrDefault(i => i.Description == "泄漏异常");
            if (leak_abnormal_point.IsChecked)
            {
                var closeHot_item = _steps.Where(i => i.Status[StatusName.热水阀] == close && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var closeCold_item = _steps.Where(i => i.Status[StatusName.冷水阀主] == close && i.Status[StatusName.冷水阀辅]==close && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                var stop_item = _steps.Where(i => i.Status[StatusName.急停按钮] == open && i.Date > _leakAbnormalDate)
                    .OrderByDescending(i => i.Date).FirstOrDefault();
                if (closeHot_item != null && closeCold_item != null && stop_item != null)
                {
                    _dicSteps.Add("处理泄漏异常", new StepDescription() { Description = leak_abnormal_point.Description + "：完成", IsRight = true });
                }
                else
                {
                    _dicSteps.Add("处理泄漏异常", new StepDescription() { Description = leak_abnormal_point.Description + "：未完成", IsRight = false });
                }
            }
            #endregion
        }
        private void ColdInHelper()
        {
            double rate = 3.6 / (77 - 22);
            Task.Run(() =>
            {
                while (!_listenCts.IsCancellationRequested)
                {
                    if (ValveInCold_State)
                    {
                        if (!Isinner) ValveInCold_item.Open();
                        FlowInCold_item.StartFlow();
                        _temperature_value -= _temperature_decrement;
                        _pressure_value -= _pressure_decrement;

                        if (_temperature_value < 90)
                        {
                            _temperature_decrement = 1;
                            _pressure_decrement = rate * _temperature_decrement;
                        }
                        if (_temperature_value < 20)
                        {
                            _temperature_decrement = 0;
                            _pressure_decrement = 0;
                        }
                        LabelPressure_item.Write(_pressure_value);
                        LabelTemperature_item.Write(_temperature_value);

                        SpPressureValue(_pressure_value);
                        SpTemperatureValue(_temperature_value);
                    }
                    else
                    {
                        if (!Isinner) ValveInCold_item.Close();
                        FlowInCold_item.StopFlow();
                    }
                    Thread.Sleep(_sleepfast);
                }
            });
        }
     
        #endregion

        #region 操纵继电器-预警
        void CloseAllRelay()
        {
            SPWriteTool(0);
        }
        /// <summary>
        /// 写继电器
        /// </summary>
        /// <param name="number">0-128， </param>
        void SPWriteTool(int number)
        {
            if (Isinner) return;
            WriteTool(_relaycmd, number);
        }
        private void StopWarning(WarningType type)
        {
            if (_warningtypes.Contains(type))
            {
                FlashWarning_item.Close();
                _relay_command = (~_warning1number) & _relay_command;
                _relay_command = (~_warning2number) & _relay_command;
                SPWriteTool(_relay_command);
                _warningtypes.Remove(type);
            }
        }

        private void BeginWarning(WarningType type)
        {
            if (!_warningtypes.Contains(type))
            {
                FlashWarning_item.Open();
                _warningtypes.Add(type);
                _relay_command = _relay_command | _warning1number;
                _relay_command = _relay_command | _warning2number;
                SPWriteTool(_relay_command);
            }
        }

        void BeginStir()
        {
            FlashWarning_item.Open();
            _relay_command = _relay_command | _stirnumber;
            //SPWriteTool(_relay_command);
        }

        void StopStir()
        {
            FlashWarning_item.Close();
            _relay_command = _relay_command & (~_stirnumber);
            //SPWriteTool(_relay_command);
        }

        void BeginLeak()
        {
            FlashWarning_item.Open();
            _relay_command = _relay_command | _fognumber;
            SPWriteTool(_relay_command);
            Thread.Sleep(500);
            StopLeak();

        }
        void StopLeak()
        {
            FlashWarning_item.Close();
            _relay_command = _relay_command & (~_fognumber);
            SPWriteTool(_relay_command);
        }

        #endregion

        #region 写串口数据
        void SpFlowValue(double value)
        {
            WriteFlowValue(_llcmd, 1, value);
            //WriteAIvalue(_aicmd, 3, value);
        }

        void SpPressureValue(double value)
        {
            WritePressureValue(_ylcmd, value);
            WriteAIvalue(_aicmd, 2, value);
        }

        void SpTemperatureValue(double value)
        {
            WriteTemperatureValue(_wdcmd, value);
            WriteAIvalue(_aicmd, 1, value);
        }
        ///
        /// <summary>
        /// 丙烯重量 左侧：1，   右侧：4
        /// </summary>
        /// <param name="index">左侧：1，   右侧：4</param>
        /// <param name="value"></param>
        void SpHeightValue(int index, double value)
        {
            if (Isinner) return;
            value = Math.Round(value, 2);
            if (value < 0) return;
            string sp_pg_value = value.ToString().PadLeft(4, '0');
            string cmd_pg = string.Format(_heightcmd, index, sp_pg_value);

            lock (this)
            {
                Thread.Sleep(150);
                if (_serialPort.IsOpen)
                    _serialPort.Write(cmd_pg);
            }
        }
        #endregion

        #region 枚举定义
        enum FieldName
        {
            //阀门
            ValveLeftEmpty, ValveOutO2, ValveInActivator, ValveInCold, ValveInColdSecond, ValveInGas, ValveInH2, ValveInHot, ValveInN2Right, ValveInN2Left, ValveInPropyleneLeft,
            ValveInPropyleneRight, ValveLeftHopper, ValveOutCold, ValveOutGas, ValveOutHot, ValveOutPropylene, ValveOutPropyleneWithHopper, ValveRightHopper, ValveInDownPropylene,
            ValveEmptyFromHopper, ValveStir, FlowOutO2, ValveStop,
            //label
            LabelPressure, LabelTemperature, LabelFlow, LabelContent, LabelPressureHopper, LabelWeight1, LabelWeight2, LabelH2press,

            //flow
            FlowInPropylene, FlowInActivator, FlowInN2, FlowInN2Left, FlowInCold, FlowInColdSecond, FlowInGas, FlowInH2, FlowInHot, FlowInLeft, FlowInPropyleneRight,
            FlowInRight, FlowOutActivator, FlowLeftEmpty, FlowOutPropylene, FlowOutCold, FlowOutGas, FlowOutHot, FlowOutGasFomKettle, FlowInPropyleneDown, FlowRightEmpty,

            //flash
            FlashKettle, FlashPressure, FlashTempreture, FlashHopperPressure,
            
            FlashWarning
        }
        enum StatusName
        {
            左侧精致丙烯进料阀, 左侧活化剂进料阀, 左侧气相丙烯回收阀, 左侧放空阀, 左侧活化剂料斗阀, 左侧氮气阀,
            氢气阀, 右侧氮气阀, 右侧去丙烯回收系统, 搅拌开关,
            右侧去高点放空总管, 右侧去丙烯回收系统从料斗, 右侧精致丙烯总管, 右侧料斗下料阀,
            冷水阀主, 冷水阀辅, 热水阀,
            冷水回水阀, 热水回水阀,
            反应釜压力, 温度, 丙烯总量, 左侧活化剂压力, 左侧丙烯重量1, 左侧丙烯重量2, 左侧氢气压力, 右侧丙烯重量,
            气相丙烯阀1, 右侧放空总管1, 气相丙烯阀2, 右侧放空总管2, 气相丙烯阀3, 右侧放空总管3,
            急停按钮,
        }

        enum WarningType
        {
            温度异常,
            压力异常,
            气相丙烯压力过高,
            通入精致丙烯过量,
            活化剂压力过高,
            反应温度过高,
            氢气压力过高,
            泄漏,
        }

     
	#endregion
    }
}

﻿using Chemistry.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry
{
    static class Global
    {
        static Global()
        {
            Examinees = new List<Examinee>(10);
            ReadConfigiguration();
            CONFIGDATA_PATH = AppDomain.CurrentDomain.BaseDirectory + "config.data";
            if (!System.IO.File.Exists(CONFIGDATA_PATH))
            {
                System.IO.File.Create(CONFIGDATA_PATH);
            }



        }

        static void ReadConfigiguration()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = config.AppSettings.Settings;
            var model = settings["Model"] !=null? settings["Model"].Value : "练习模式";
            RunModel = (RunModel)Enum.Parse(typeof(RunModel), model);

            SERVER_URL = config.AppSettings.Settings["ChemistryServer"].Value;
        }

        public static string SERVER_URL;
        public static readonly string CONFIGDATA_PATH;
        public static string CurrentWorkflowName;

        public static RunModel RunModel;

        public static int Random;
        public static List<Examinee> Examinees;

        public static int PrintIndex =1;
        public static string ClientName;
        public static int LogSerial;

        public static Client Client;

        
 
    }


    public class SuperQuqesionApiMode
    {
        public int status { get; set; }
        //public List<SuperQuestion> superquestions { get; set; }
    }
}
